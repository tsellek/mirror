[![pipeline status](https://gitlab.com/tsellek/mirror/badges/master/pipeline.svg)](https://gitlab.com/tsellek/mirror/commits/master)

# mirror - a C++ Reflection Library

## Overview

Mirror enables developers to perform reflection on existing code without 
modifying it and without running it through any kind of preprocessor. It does 
that by using the debug information that is stored alongside the executable 
code of the program.

### Features
 - Explore the names and types of members and methods via iteration over their 
 descriptors
 - Dynamically create new objects of a given type from its metatype
 - Obtain the value of member variables by name
 - Invoke methods by name

### Limitations
 - First and foremost - Mirror relies on debug information being present. If a module (executable or library) was compiled with no debug information, or with insufficient debug information (that is, no type information), or the debug information was compiled to a separate file (such as a .PDB file on Windows) that is not available, reflection will not be available on any of the types from that module. 
 - Mirror does not account for inheritance at this time
 - Mirror is not able to call methods that were inlined or optimized away
 - Mirror is not able to get the value of data members that are not in memory
 - When calling a method by name, if you want to pass arguments that are of a non-POD type your compiler needs to support non-POD arguments to variadic functions (C++23 standard [7.6.1.3/11]). CLang explicitly does not support this (and will issue a warning to that effect), GCC does support this (documented [here](https://gcc.gnu.org/onlinedocs/gcc/Conditionally-supported-behavior.html#Conditionally-supported-behavior) ), MSVC seems to support this but it is not documented.

## Getting Started

### IMPROTANT USAGE NOTE
Please keep in mind that Mirror does not do any automatic conversions when passing parameters to methods. Some examples that will **NOT** work:
 - Passing an _std::string_ to a method taking _const std::string&_ (or any type to a method taking a reference to the type)
 - C-like automatic conversions, like passing an _int_ to a method taking a _long_

Attempting to do any of the above will lead to an exception being thrown. 

### Example

```
#include "Mirror/Reflection.hpp"

template<typename T>
unsigned
sampleFunction(T& obj)
{
    auto metaclass = meta_cast(obj);

    //First try the really cool method if it's there
    std::vector<Method> methods = metaclass.Methods("ReallyCoolMethod");
    if (methods.size() > 0)
    {
        if (methods[0].Arguments()[0].GetType().IsPointer() &&
            methods[0].Arguments()[0].GetType().IsConst())
        {
            return methods[0].CallAs<unsigned>("blah");
        }
    }
```

### Additional examples

See the 'samples' subfolder for usage examples

## Runtime Dependencies

### Linux
Mirror depends on libdwarf to read type information from Linux ELF executables

### Windows
Mirror depends on Microsoft's DIA SDK (msdiaXXX.dll where XXX may be 80, 100 or 
140 depending on what Mirror was compiled against). The COM interfaces provided
by this DLL must be registered by calling:
```
regsvr32 <path_to_dll>
```

## Platforms Supported

 - Linux, 64bit ELF+DWARF, **CLang is not fully supported (see limitations above)**
 - Windows, 64bit PDB

## Building Mirror

Mirror uses Conan for dependencies management and CMake as a build system.
To install all dependencies and build the static+debug libmirror library do:
1. `conan install . --output-filter=build --build=missing -s build_type=Debug`
2. `cd build`
3. `cmake --preset=conan-debug -S .. -B .`
4. `cmake --build .. --preset=conan-debug`

### Build Arguments
 - -DBUILD_SAMPLES=1 will build the samples under the 'samples' directory. This 
 includes 'pdbdump' when building on Windows.
 - -DBUILD_TESTS=1 will build the unit tests implemented in the 'test' directory. See note below when building on Windows.
 - -DDIA_SDK_ROOT=<path> is required when building on Windows. It should point to the directory that includes the DIA SDK 'include' and 'lib' directories (e.g. C:/Program Files (x86)/Microsoft Visual Studio/2017/BuildTools/DIA SDK)
 - -Dgtest_force_shared_crt=ON is required when building the unit tests on Windows

### Dependencies

In addition to , Mirror depends on the following libraries:

#### Windows
 - Microsoft's DIA SDK
 - Boost

#### Linux
 - libdwarf
 - Boost (for samples only)


## Reference

### Namespace
Mirror's API lives in the 'meta' namespace. Anything under the 'meta::details' should not be used and is subject to change.

### Initialization

There is no need to explicitly initialize. Mirror will initialize itself (which
may take some time) the first time *meta_cast* is called.

### meta_cast

To obtain the metaclass for a given type, object or variable call one of:

```
meta_cast<Type>()
```
or
```
meta_cast(object)
```
or
```
meta_cast("<class name>")
```

### Handling Errors

On failure Mirror APIs throw exceptions that inherit from *reflection_error*

### Full Reference

See the 'doc' subfolder for full documentation

