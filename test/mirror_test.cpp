#include "gtest/gtest.h"
#include "Mirror/Reflection.hpp"

#include <boost/algorithm/string.hpp>

using namespace meta;

using namespace boost::algorithm;

using namespace std::string_literals;

template<typename T, typename Pred>
bool exists(std::vector<T> vec, Pred p)
{
    return std::find_if(vec.begin(), vec.end(), p) != vec.end();
}

class EmptyClass
{        
};

class ClassWithMembers
{
public:
    int PodPublicMember=1;
    EmptyClass ClassPublicMember;

private:
    int PrivateMember=2;
    const std::string ConstPrivateMember=""s;
};

class ClassWithMethods
{
public:
    void PublicMethod();
    void PublicMethodWithParams(int x);
    std::string PublicMethodWithParamAndReturnValue(std::string s);

public:
    int LastParam = 0;

private:
    ClassWithMembers PrivateMethod(const std::string& s, unsigned* x);
};

void ClassWithMethods::PublicMethod()
{

}

void ClassWithMethods::PublicMethodWithParams(int x)
{
    LastParam = x;
}
std::string ClassWithMethods::PublicMethodWithParamAndReturnValue(std::string s)
{
    return s;
}

class SuperclassWithVirtualMethods
{
private:
    virtual void Method() = 0;
};

class SubclassWithVirtualMethods: public SuperclassWithVirtualMethods
{
private:
    virtual void Method() override
    {}
};

class ClassWithConstructor
{
public:
    ClassWithConstructor(int v1, const char* v2)
    :Member1(v1), Member2(v2)
    {}

public:
    int Member1;
    std::string Member2;
};

TEST(Mirror, name_matches_class_name)
{
    EXPECT_EQ("EmptyClass", meta_cast<EmptyClass>().Name());
}


TEST(Mirror, empty_class_has_size_1)
{
    EXPECT_EQ(1, meta_cast<EmptyClass>().Size());
}

TEST(Mirror, nonempty_class_has_correct_size)
{
    EXPECT_EQ(sizeof(ClassWithMembers), meta_cast<ClassWithMembers>().Size());
}

TEST(Mirror, class_has_right_type_traits)
{
    auto metaclass = meta_cast<EmptyClass>();
    
    EXPECT_FALSE(metaclass.IsConst());
    EXPECT_FALSE(metaclass.IsVolatile());
    EXPECT_FALSE(metaclass.IsPointer());
    EXPECT_FALSE(metaclass.IsReference());
    EXPECT_FALSE(metaclass.IsTypedef());
    
}

TEST(Mirror, object_type_name_is_same_as_class)
{
    EmptyClass obj;
    EXPECT_EQ("EmptyClass", meta_cast(obj).Name());
}

TEST(Mirror, members_have_the_right_name)
{
    ClassWithMembers obj;
    auto members = meta_cast(obj).Members();

    EXPECT_EQ(4, members.size());

    EXPECT_TRUE(exists(members, [](const DataIdentifier& v){return v.Name() == "PodPublicMember";} ));
    EXPECT_TRUE(exists(members, [](const DataIdentifier& v){return v.Name() == "ClassPublicMember";} ));

    EXPECT_EQ("PrivateMember"s, meta_cast(obj).MemberByName("PrivateMember")->Name());
}

TEST(Mirror, class_has_right_number_of_members)
{

    ClassWithMembers obj;
    auto members = meta_cast(obj).Members();

    EXPECT_EQ(4, members.size());
}

TEST(Mirror, members_have_the_right_size)
{
    ClassWithMembers obj;

    auto metaclass = meta_cast(obj);

    EXPECT_EQ(sizeof(ClassWithMembers::PodPublicMember), metaclass.MemberByName("PodPublicMember")->Size());
    EXPECT_EQ(sizeof(ClassWithMembers::ClassPublicMember), metaclass.MemberByName("ClassPublicMember")->Size());

}

TEST(Mirror, members_have_the_right_type)
{
    ClassWithMembers obj;

    auto metaclass = meta_cast(obj);

    auto m1 = metaclass.MemberByName("PodPublicMember");
    EXPECT_EQ("int", m1->GetType().Name());
    EXPECT_FALSE(m1->GetType().IsConst());
    EXPECT_FALSE(m1->GetType().IsVolatile());
    EXPECT_FALSE(m1->GetType().IsReference());
    EXPECT_FALSE(m1->GetType().IsPointer());
    EXPECT_FALSE(m1->GetType().IsTypedef());

    auto m2 = metaclass.MemberByName("ClassPublicMember");
    EXPECT_EQ("EmptyClass", m2->GetType().Name());
    EXPECT_FALSE(m2->GetType().IsConst());
    EXPECT_FALSE(m2->GetType().IsVolatile());
    EXPECT_FALSE(m2->GetType().IsReference());
    EXPECT_FALSE(m2->GetType().IsPointer());
    EXPECT_FALSE(m2->GetType().IsTypedef());


    auto m3 = metaclass.MemberByName("ConstPrivateMember");
    EXPECT_TRUE(m3->GetType().IsConst());
    EXPECT_FALSE(m3->GetType().IsVolatile());
    EXPECT_FALSE(m3->GetType().IsReference());
    EXPECT_FALSE(m3->GetType().IsPointer());
    ASSERT_TRUE(m3->GetType().IsTypedef());

    EXPECT_TRUE(contains(m3->GetType().Name(), "string"));
    EXPECT_TRUE(contains(m3->GetType().UnderlyingType().Name(), "basic_string"));
}

TEST(Mirror, members_have_the_right_value)
{
    ClassWithMembers obj;

    auto metaclass = meta_cast(obj);

    EXPECT_EQ(1, metaclass.MemberByName("PodPublicMember")->ValueAs<int>());
    EXPECT_EQ(2, metaclass.MemberByName("PrivateMember")->ValueAs<int>());

}

TEST(Mirror, members_have_the_right_access)
{
    ClassWithMembers obj;


    auto metaclass = meta_cast(obj);

    EXPECT_EQ(Accessibility::Public, metaclass.MemberByName("PodPublicMember")->Access());
    EXPECT_EQ(Accessibility::Private, metaclass.MemberByName("PrivateMember")->Access());
}


TEST(Mirror, methods_have_the_right_name)
{
    ClassWithMethods obj;
    EXPECT_FALSE(meta_cast(obj).Methods("PublicMethod").empty());
    EXPECT_FALSE(meta_cast(obj).Methods("PrivateMethod").empty());
}


TEST(Mirror, methods_have_the_right_return_type)
{
    ClassWithMethods obj;
    auto method1 = meta_cast(obj).Methods("PublicMethod").at(0);
    EXPECT_EQ("void", method1.ReturnType().Name());

    auto method2 = meta_cast(obj).Methods("PrivateMethod").at(0);
    EXPECT_EQ("ClassWithMembers", method2.ReturnType().Name());
}

TEST(Mirror, methods_have_the_right_access)
{
    ClassWithMethods obj;
    auto method1 = meta_cast(obj).Methods("PublicMethod").at(0);
    EXPECT_EQ(Accessibility::Public, method1.Access());

    auto method2 = meta_cast(obj).Methods("PrivateMethod").at(0);
    EXPECT_EQ(Accessibility::Private, method2.Access());
}

TEST(Mirror, methods_arguments_have_the_right_types)
{
    ClassWithMethods obj;
    auto method = meta_cast(obj).Methods("PrivateMethod").at(0);
    auto args = method.Arguments();
    ASSERT_EQ(args.size(), 2);
    //EXPECT_TRUE(exists(args, [](const DataIdentifier& v){return contains(v.GetType().Name(), "string") &&  ends_with(v.GetType().Name(), "const &");}));
    EXPECT_TRUE(exists(args, [](const DataIdentifier& v){return contains(v.GetType().Name(), "string") &&  v.GetType().IsReference();}));
    EXPECT_TRUE(exists(args, [](const DataIdentifier& v){return v.GetType().Name() == "unsigned int *";}));

}

TEST(Mirror, type_is_dynamic_type)
{
    auto dyntype = std::make_shared<SubclassWithVirtualMethods>();

    auto metaclass_dyntype = meta_cast(*dyntype.get());

    EXPECT_EQ("SubclassWithVirtualMethods", metaclass_dyntype.Name());
}

TEST(Mirror, created_object_constructor_is_called_correctly)
{
    auto metaclass = meta_cast<ClassWithConstructor>();

    ClassWithConstructor* obj = metaclass.Create(42, "Test");
    EXPECT_EQ(42, obj->Member1);
    EXPECT_EQ("Test", obj->Member2);
    delete obj;
}

TEST(Mirror, can_call_method_by_name)
{
    ClassWithMethods object;

    auto method = meta_cast(object).Methods("PublicMethod").at(0);
    EXPECT_NO_THROW(method.CallAs<void>());
}

TEST(Mirror, can_call_method_with_params_by_name)
{
    ClassWithMethods object;

    auto method = meta_cast(object).Methods("PublicMethodWithParams"s).at(0);

    EXPECT_NO_THROW(method.CallAs<void>(42));
    EXPECT_EQ(42, object.LastParam);
}

TEST(Mirror, can_call_method_with_params_and_return_value_by_name)
{
    ClassWithMethods object;

    auto method = meta_cast(object).Methods("PublicMethodWithParamAndReturnValue"s).at(0);

    EXPECT_EQ("Test"s, method.CallAs<std::string>("Test"s));
}

TEST(Mirror, cant_call_method_with_wrong_return_type)
{
    ClassWithMethods object;

    auto method = meta_cast(object).Methods("PublicMethodWithParamAndReturnValue"s).at(0);
    EXPECT_THROW(method.CallAs<int>("Test"s), reflection_error);
}

TEST(Mirror, cant_call_method_with_wrong_num_of_params)
{    
    ClassWithMethods object;

    auto method = meta_cast(object).Methods("PublicMethodWithParamAndReturnValue"s).at(0);
    EXPECT_THROW(method.CallAs<std::string>("Test"s, 1), reflection_error);
    EXPECT_THROW(method.CallAs<std::string>(), reflection_error);
}

TEST(Mirror, cant_call_method_with_wrong_type_of_params)
{
    ClassWithMethods object;

    auto method = meta_cast(object).Methods("PublicMethodWithParamAndReturnValue"s).at(0);
    EXPECT_THROW(method.CallAs<std::string>('c'), reflection_error);
}

TEST(Mirror, can_meta_cast_fundamental_type)
{
    Type type = meta_cast<int>();
    EXPECT_EQ("int", type.Name());
    EXPECT_EQ(sizeof(int), type.Size());


    EXPECT_FALSE(type.IsConst());
    EXPECT_FALSE(type.IsVolatile());
    EXPECT_FALSE(type.IsPointer());
    EXPECT_FALSE(type.IsReference());
    EXPECT_FALSE(type.IsTypedef());
}

TEST(Mirror, can_meta_cast_variable)
{
    float f = 0.5;
    Variable var = meta_cast(f);
    EXPECT_EQ("float", var.GetType().Name());
    EXPECT_EQ(sizeof(float), var.GetType().Size());


    EXPECT_FALSE(var.GetType().IsConst());
    EXPECT_FALSE(var.GetType().IsVolatile());
    EXPECT_FALSE(var.GetType().IsPointer());
    EXPECT_FALSE(var.GetType().IsReference());
    EXPECT_FALSE(var.GetType().IsTypedef());
}

TEST(Mirror, can_meta_cast_by_name)
{
    Class meta_class = meta_cast("EmptyClass");
    EXPECT_EQ("EmptyClass", meta_class.Name());
}