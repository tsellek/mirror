var annotated_dup =
[
    [ "meta", "namespacemeta.html", [
      [ "Class", "classmeta_1_1_class.html", "classmeta_1_1_class" ],
      [ "ClassOf", "classmeta_1_1_class_of.html", "classmeta_1_1_class_of" ],
      [ "DataIdentifier", "classmeta_1_1_data_identifier.html", "classmeta_1_1_data_identifier" ],
      [ "Member", "classmeta_1_1_member.html", "classmeta_1_1_member" ],
      [ "MetaObject", "classmeta_1_1_meta_object.html", "classmeta_1_1_meta_object" ],
      [ "Method", "classmeta_1_1_method.html", "classmeta_1_1_method" ],
      [ "Parameter", "classmeta_1_1_parameter.html", "classmeta_1_1_parameter" ],
      [ "reflection_error", "classmeta_1_1reflection__error.html", "classmeta_1_1reflection__error" ],
      [ "SourceLocation", "structmeta_1_1_source_location.html", "structmeta_1_1_source_location" ],
      [ "Type", "classmeta_1_1_type.html", "classmeta_1_1_type" ],
      [ "Variable", "classmeta_1_1_variable.html", "classmeta_1_1_variable" ]
    ] ]
];