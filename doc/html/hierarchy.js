var hierarchy =
[
    [ "meta::ClassOf< ClassType >", "classmeta_1_1_class_of.html", null ],
    [ "meta::MetaObject", "classmeta_1_1_meta_object.html", [
      [ "meta::DataIdentifier", "classmeta_1_1_data_identifier.html", [
        [ "meta::Member", "classmeta_1_1_member.html", null ],
        [ "meta::Parameter", "classmeta_1_1_parameter.html", null ],
        [ "meta::Variable", "classmeta_1_1_variable.html", null ]
      ] ],
      [ "meta::Method", "classmeta_1_1_method.html", null ],
      [ "meta::Type", "classmeta_1_1_type.html", [
        [ "meta::Class", "classmeta_1_1_class.html", null ]
      ] ]
    ] ],
    [ "std::runtime_error", null, [
      [ "meta::reflection_error", "classmeta_1_1reflection__error.html", null ]
    ] ],
    [ "meta::SourceLocation", "structmeta_1_1_source_location.html", null ]
];