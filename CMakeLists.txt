cmake_minimum_required (VERSION 3.15)
 
project (mirror)



set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if(UNIX AND NOT APPLE AND NOT CYGWIN)
    if(${CMAKE_SYSTEM_NAME} MATCHES ".*[Ll][Ii][Nn][Uu][Xx].*" )
        set (LINUX 1)
    endif()
endif()

if(LINUX)
    set(BACKEND_FILES
    "Mirror/details/DWARF/DebugInfoParser_DWARF.cpp"
    "Mirror/details/DWARF/Die.cpp"
    "Mirror/details/DWARF/Dwarf.cpp"
    "Mirror/details/DWARF/LinuxProcess.cpp"
    "Mirror/details/DWARF/dwarf_ptr.cpp"
    )
elseif(WIN32)
    set(BACKEND_FILES
    "Mirror/details/PDB/Pdb.cpp"
    "Mirror/details/PDB/DebugInfoParser_PDB.cpp"
    "Mirror/details/PDB/Symbol.cpp"
    "Mirror/details/PDB/WindowsProcess.cpp"
    "Mirror/details/PDB/pdb_error.cpp")
endif()

add_library(mirror

${BACKEND_FILES}

Mirror/MetaObject.cpp
Mirror/Class.cpp
Mirror/Method.cpp
Mirror/DataIdentifier.cpp
Mirror/Member.cpp
Mirror/Parameter.cpp
Mirror/Variable.cpp
Mirror/Type.cpp
Mirror/types.cpp

Mirror/details/Mirror.cpp
Mirror/details/Process.cpp
)

if (LINUX)
    find_package(libdwarf REQUIRED)
    target_include_directories(mirror PUBLIC ${libdwarf_INCLUDE_DIR})

    target_compile_options(mirror PRIVATE -Wall -Wextra -pedantic -Werror)

    target_link_libraries(mirror libdwarf::libdwarf)
    target_link_libraries(mirror stdc++fs)
elseif (WIN32)
    find_package(Boost REQUIRED)
    target_include_directories(mirror PUBLIC ${Boost_INCLUDE_DIRS})
    string(REGEX REPLACE "/W[0-4]" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    target_compile_options(mirror PRIVATE /W4 /WX)
    
    target_include_directories(mirror PRIVATE "${DIA_SDK_ROOT}/include")

    target_link_libraries(mirror "${DIA_SDK_ROOT}/lib/amd64/diaguids.lib")

endif()


if (BUILD_SAMPLES)
    find_package(Boost REQUIRED)
    target_include_directories(mirror PUBLIC ${Boost_INCLUDE_DIRS})
    enable_testing()
    add_subdirectory(samples)
endif()

if (BUILD_TESTS)
    enable_testing()
    add_subdirectory(test)
endif()