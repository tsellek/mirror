
#include <string>


using namespace std::string_literals;

class SampleClass
{
public:
    SampleClass(int m1, std::string m2);

    void SomeMethod(unsigned);
    unsigned SomeOtherMethod(const std::string&);

    int GetMember1();

    const std::string& GetMember2();

private:
    int member1 = 0;
    std::string member2 = "string"s;
};

class SampleSubClass : public SampleClass
{
public:
    SampleSubClass(int m1, std::string m2, int m3)
    :SampleClass(m1, m2), member3(m3)
    {

    }

private:
    int member3;
};

class ClassWithVirtualMethods
{
public:
    virtual ~ClassWithVirtualMethods()
    {}
};

class SubclassWithVirtualMethods : public ClassWithVirtualMethods
{

};
