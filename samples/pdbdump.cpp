#include "Mirror/details/PDB/Pdb.hpp"

#include "Mirror/details/PDB/Symbol.hpp"

#include <string>
#include <iostream>
#include <vector>

using namespace meta::details;

Pdb pdb{L"rto.exe"};

const std::vector<const char *> rgTags  =
{
  "(SymTagNull)",                     // SymTagNull
  "Executable (Global)",              // SymTagExe
  "Compiland",                        // SymTagCompiland
  "CompilandDetails",                 // SymTagCompilandDetails
  "CompilandEnv",                     // SymTagCompilandEnv
  "Function",                         // SymTagFunction
  "Block",                            // SymTagBlock
  "Data",                             // SymTagData
  "Annotation",                       // SymTagAnnotation
  "Label",                            // SymTagLabel
  "PublicSymbol",                     // SymTagPublicSymbol
  "UserDefinedType",                  // SymTagUDT
  "Enum",                             // SymTagEnum
  "FunctionType",                     // SymTagFunctionType
  "PointerType",                      // SymTagPointerType
  "ArrayType",                        // SymTagArrayType
  "BaseType",                         // SymTagBaseType
  "Typedef",                          // SymTagTypedef
  "BaseClass",                        // SymTagBaseClass
  "Friend",                           // SymTagFriend
  "FunctionArgType",                  // SymTagFunctionArgType
  "FuncDebugStart",                   // SymTagFuncDebugStart
  "FuncDebugEnd",                     // SymTagFuncDebugEnd
  "UsingNamespace",                   // SymTagUsingNamespace
  "VTableShape",                      // SymTagVTableShape
  "VTable",                           // SymTagVTable
  "Custom",                           // SymTagCustom
  "Thunk",                            // SymTagThunk
  "CustomType",                       // SymTagCustomType
  "ManagedType",                      // SymTagManagedType
  "Dimension",                        // SymTagDimension
  "CallSite",                         // SymTagCallSite
  "InlineSite",                       // SymTagInlineSite
  "BaseInterface",                    // SymTagBaseInterface
  "VectorType",                       // SymTagVectorType
  "MatrixType",                       // SymTagMatrixType
  "HLSLType",                         // SymTagHLSLType
  "Caller",                           // SymTagCaller,
  "Callee",                           // SymTagCallee,
  "Export",                           // SymTagExport,
  "HeapAllocationSite",               // SymTagHeapAllocationSite
  "CoffGroup",                        // SymTagCoffGroup
  "Inlinee",                          // SymTagInlinee
};

void dumpSymbol(Symbol s, int depth=0)
{
    if (depth > 12)
    {
        std::clog<<std::string(depth, ' ')<<"Exceeded max depth. Stopping."<<std::endl;
        return;
    }
    std::clog<< std::string(depth, ' ')<<"["<<rgTags.at(s.Tag())<<"]"<<
                " '"<<s.Name()<<"'"<<std::endl;
    try{
        Symbol typesym = pdb.SymbolById((DWORD)s.GetType());
        std::clog<< std::string(depth+2, ' ')<<"==[TYPE]==>"<<std::endl;
        dumpSymbol(typesym, depth+2);
        std::clog<< std::string(depth+2, ' ')<<"<==[ENDTYPE("<<s.Name()<<")]=="<<std::endl;
    }
    catch(std::exception&)
    {
    }

    for (Symbol s:pdb.Children(s.Id()))
    {
        dumpSymbol(s, depth+1);
    }
}

int main(int argc, char* argv[])
{
    for (Symbol s:pdb.GlobalTypes())
    {
        if (s.Name() == argv[1])
            dumpSymbol(s);
    }
}