#include "PrintClassMetadata.hpp"

#include "SampleClass.hpp"

#include "Mirror/Class.hpp"
#include "Mirror/Reflection.hpp"

#include <iostream>

#include <cassert>
#include <string>

using namespace std::string_literals;
using namespace meta;

int main(){


    try{        
        auto metaclass = meta_cast<SampleClass>();
        SampleClass* newobject = metaclass.Create(666, "Where did this object come from?"s);

        std::cout<<newobject->GetMember1()<<", "<<newobject->GetMember2()<<std::endl;

        assert(newobject->GetMember1() == 666);
        assert(newobject->GetMember2() == "Where did this object come from?"s);

    }
    catch (std::exception& e)
    {
        std::cerr<<e.what()<<std::endl;
        return 1;
    }
    return 0;
}