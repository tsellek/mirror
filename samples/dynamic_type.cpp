#include "PrintClassMetadata.hpp"

#include "SampleClass.hpp"

#include "Mirror/Class.hpp"
#include "Mirror/Reflection.hpp"

#include <iostream>
#include <memory>

#include <cassert>

using namespace meta;

int main(){

    try{
        std::shared_ptr<SampleClass> no_dyntype_info = std::make_shared<SampleSubClass>(777, "sampletext"s, 42);

        auto metaclass_nodyntype_info = meta_cast<>(*no_dyntype_info.get());

        std::clog<<metaclass_nodyntype_info.Name()<<std::endl;

        assert(metaclass_nodyntype_info.Name() == "SampleClass");

        auto dyntype = std::make_shared<SubclassWithVirtualMethods>();

        auto metaclass_dyntype = meta_cast(*dyntype.get());

        std::clog<<metaclass_dyntype.Name()<<std::endl;

        assert(metaclass_dyntype.Name() == "SubclassWithVirtualMethods");

    }
    catch (std::exception& e)
    {
        std::cerr<<e.what()<<std::endl;
        return 1;
    }
    return 0;
}
