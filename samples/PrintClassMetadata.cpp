#include "PrintClassMetadata.hpp"

#include <boost/algorithm/string.hpp>

#include <iostream>

using namespace meta;

void printClassMetadata(const Class& c)
{
    std::cout<<c.Name()<<"("<<c.Size()<<" bytes)"<<std::endl;
    
    std::cout<<"Methods:"<<std::endl;
    for (auto m: c.Methods()){
        std::cout<<"    ["<<m.Access()<<"] "<<m.Name();

        auto args = m.Arguments();
        std::vector<std::string> argtypes;
        std::transform(args.begin(), args.end(), std::back_inserter(argtypes), [](const DataIdentifier& v){return v.GetType().Name();});

        std::cout<<"("<<boost::join(argtypes, ", ")<<")";

        if (!m.ReturnType().Name().empty()){
            std::cout<<" -> "<<m.ReturnType().Name();
        } 
        std::cout<<std::endl;
    }
    std::cout<<"Members:"<<std::endl;
    for (auto m: c.Members()){
        std::cout<<"    ["<<m.Access()<<"] "<<m.GetType().Name()<<" "<<m.Name()<<std::endl;
    }
}
