#include "Mirror/Reflection.hpp"
#include "SampleClass.hpp"

#include "Mirror/Method.hpp"

#include <optional>
#include <algorithm>
#include <cassert>

using namespace meta;

template<typename T>
unsigned
sampleFunction(T& obj)
{
    auto metaclass = meta_cast(obj);

    //First try the really cool method if it's there
    std::vector<Method> methods1 = metaclass.Methods("ReallyCoolMethod");
    if (methods1.size() > 0)
    {
        assert(methods1.size() == 1);
        unsigned ret = methods1[0].CallAs<unsigned>();
        return 1;
    }

    //Object doesn't have the really cool method. Try some other one.
    std::vector<Method> methods2 = metaclass.Methods("SomeOtherMethod");
    if (methods2.size() > 0)
    {
        assert(methods2.size() == 1);
        auto method = methods2[0];
        //But wait! are we sure the arguments have the expected types?
        if (method.Arguments()[0].GetType().Name().find("string") != std::string::npos)
        {
            //What about the return type?
            if (method.ReturnType().Name() == "unsigned int")
            {
                return method.CallAs<unsigned>("Some string");
            }
        }
    }
    return 2;
}

int main()
{
    SampleClass obj(1,"");
    unsigned ret = sampleFunction(obj);

    assert(ret == 0);

    return 0;
}