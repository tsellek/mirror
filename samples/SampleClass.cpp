#include "SampleClass.hpp"

SampleClass::SampleClass(int m1, std::string m2)
:member1(m1), member2(m2)
{

}

void SampleClass::SomeMethod(unsigned)
{
    
}

unsigned SampleClass::SomeOtherMethod(const std::string&)
{
    return 0;
}

int SampleClass::GetMember1()
{
    return member1;
}

const std::string& SampleClass::GetMember2()
{
    return member2;
}