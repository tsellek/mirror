#include "PrintClassMetadata.hpp"

#include "Mirror/Reflection.hpp"

#include <iostream>

using namespace meta;
 
int main(){

    try{
        printClassMetadata(meta_cast("meta::Class"));
    }
    catch (std::exception& e)
    {
        std::cerr<<e.what()<<std::endl;
        return 1;
    }
    return 0;
}