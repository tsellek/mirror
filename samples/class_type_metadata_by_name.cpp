#include "PrintClassMetadata.hpp"

#include "SampleClass.hpp"

#include "Mirror/Reflection.hpp"

#include <iostream>

using namespace meta;
 
int main(){

    try{
        auto metaclass = meta_cast("SampleClass");

        printClassMetadata(metaclass);
    }
    catch (std::exception& e)
    {
        std::cerr<<e.what()<<std::endl;
        return 1;
    }
    return 0;
}