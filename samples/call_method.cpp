#include "SampleClass.hpp"

#include "Mirror/Class.hpp"
#include "Mirror/Reflection.hpp"

#include <iostream>
#include <cassert>

using namespace meta;

int main(){

    try{

        SampleClass object(777, "sampletext"s);

        auto metaclass = meta_cast<>(object);

        auto methods = metaclass.Methods("GetMember1");
        assert(methods.size() == 1);
        Method GetMember = methods[0];

        assert(GetMember.Name()=="GetMember1");

        unsigned ret = GetMember.CallAs<unsigned>();
        std::cout<<"object.member1 == "<<ret<<std::endl;
        assert(ret == 777);

        bool bad_call = false;
        try
        {
            GetMember.CallAs<std::string>();
        }
        catch(reflection_error&)
        {
            bad_call = true;
        }
        assert(bad_call);

        bad_call = false;
        try
        {
            GetMember.CallAs<unsigned>(7);
        }
        catch(reflection_error&)
        {
            bad_call = true;
        }
        assert(bad_call);
    }

    catch (std::exception& e)
    {
        std::cerr<<e.what()<<std::endl;
        return 1;
    }
    return 0;
}