#include "PrintClassMetadata.hpp"

#include "SampleClass.hpp"

#include "Mirror/Class.hpp"
#include "Mirror/Reflection.hpp"

#include <iostream>
#include <cassert>

using namespace meta;

int main(){

    try{
        SampleClass object(777, "sampletext"s);

        auto metaclass = meta_cast<>(object);

        printClassMetadata(metaclass.GetUntyped());

        auto members = metaclass.Members();
        for (auto m: members)
        {
            if (m.Name()=="member1")
            {
                std::cout<<"object.member1 == "<<m.ValueAs<unsigned>()<<std::endl;
                assert(m.ValueAs<unsigned>() == 777);
            }
            else if (m.Name()=="member2")
            {
                std::cout<<"object.member2 == "<<m.ValueAs<std::string>()<<std::endl;
                assert(m.ValueAs<std::string>() == "sampletext"s);
            }
        }

    }
    catch (std::exception& e)
    {
        std::cerr<<e.what()<<std::endl;
        return 1;
    }
    return 0;
}