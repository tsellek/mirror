#include "types.hpp"

using namespace std::string_literals;

namespace meta
{

reflection_error::reflection_error(const std::string& what)
:std::runtime_error("libmirror: reflection error ("s+what+")"s)
{

}

std::ostream& operator<<(std::ostream& os, Accessibility access)
{
    switch(access)
    {
        case Accessibility::Private:
        os<<"Private";
        break;
        case Accessibility::Protected:
        os<<"Protected";
        break;
        case Accessibility::Public:
        os<<"Public";
        break;
        default:
        os<<"!Unknown!";
        break;
    };

    return os;
}

std::ostream& operator<<(std::ostream& os, const SourceLocation& sl)
{
    os<<sl.file<<":"<<sl.line;
    return os;
}

bool operator==(const SourceLocation& sl1, const SourceLocation& sl2)
{
    return std::filesystem::equivalent(sl1.file, sl2.file) && (sl1.line == sl2.line);
}

bool operator!=(const SourceLocation& sl1, const SourceLocation& sl2)
{
    return !(sl1 == sl2);
}

bool operator<(const SourceLocation& sl1, const SourceLocation& sl2)
{
    if (sl1.file < sl2.file)
    {
        return true;
    }
    else
    {
        return (sl1.line < sl2.line);
    }
}
}