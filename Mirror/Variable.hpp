#pragma once

#include "DataIdentifier.hpp"

namespace meta
{

class Variable : public DataIdentifier
{
public:
    Variable(void* addr, uint64_t dbginfoid, details::DebugInfoFetcher* fetcher);

    virtual const Type GetType() const;

protected:
    virtual void* Address() const override;

private:
    void* _addr;
    Type _type;
};

}