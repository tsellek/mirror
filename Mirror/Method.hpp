#pragma once

#include "MetaObject.hpp"
#include "Type.hpp"
#include "Parameter.hpp"
#include <vector>

#include "details/VerifyMethodCall.hpp"

namespace meta
{

class Method : public MetaObject
{
public:
    Method(void* This, uint64_t dbginfoid, details::DebugInfoFetcher* fetcher);
    Type ReturnType() const;
    std::vector<Parameter> Arguments() const;

    template<typename RetType, class... Args> 
    RetType CallAs(Args... args) const
    {
        typedef RetType (*MethodWrapper)(void*, ...);
        if (!_This)
        {
            throw reflection_error("Calling method with NULL 'this' pointer");
        }

        auto expected_args = Arguments();
        details::VerifyMethodCall<RetType>(ReturnType(), expected_args.begin(), expected_args.end(), args...); //not validating 'this' pointer

        MethodWrapper callable = (MethodWrapper)(_infofetcher->SubroutineEntry(_dbginfoid));
        return callable(_This, args...);
    }

    template<typename T> friend class MethodOf;
private:
    void* _This;
};

}