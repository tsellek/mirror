#include "Member.hpp"

namespace meta
{
    Member::Member(void* This, uint64_t dbginfoid, details::DebugInfoFetcher* fetcher)
    :DataIdentifier(dbginfoid, fetcher), _This(This)
    {
        
    }

    void* Member::Address() const
    {
        if (!_This)
        {
            throw reflection_error("Trying to get the value of a member with a NULL 'this' pointer");
        }

        size_t offset = _infofetcher->MemberOffset(_dbginfoid);
        return ((char*)_This)+offset;
    }

}