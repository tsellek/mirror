#include "MetaObject.hpp"

namespace meta
{

using namespace details;

MetaObject::MetaObject(uint64_t dbginfoid, DebugInfoFetcher* fetcher)
:_infofetcher(fetcher), _dbginfoid(dbginfoid)
{

}

std::string MetaObject::Name() const
{
    return _infofetcher->Name(_dbginfoid);
}

size_t MetaObject::Size() const
{
    return _infofetcher->Size(_dbginfoid);
}

Accessibility MetaObject::Access() const
{
    return _infofetcher->Access(_dbginfoid);
}

MetaObject::~MetaObject()
{

}

}