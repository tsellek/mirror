#include "DataIdentifier.hpp"
#include <iostream>

namespace meta
{

using namespace details;

DataIdentifier::DataIdentifier(uint64_t dbginfoid, DebugInfoFetcher* fetcher)
:MetaObject(dbginfoid, fetcher)
{
}


const Type DataIdentifier::GetType() const
{
    uint64_t type_dbginfoid = _infofetcher->TypeOf(_dbginfoid);
    return Type(type_dbginfoid, _infofetcher);
}



}
