#pragma once

#include <Dia2.h>

#include <string>
#include <vector>

namespace meta::details
{

class Symbol;


class Pdb
{
public:
    Pdb(const std::wstring& path);
    ~Pdb();

    std::vector<Symbol> GlobalTypes();
    Symbol SymbolById(DWORD);
    std::vector<Symbol> Methods(DWORD id);
    std::vector<Symbol> Arguments(DWORD id);
    std::vector<Symbol> Members(DWORD id);

    std::vector<Symbol> Children(DWORD id);

private:
    IDiaSymbol* DiaSymbolById(DWORD);
    std::vector<Symbol> findChildren(IDiaSymbol* sym, enum SymTagEnum kind);

private:
    IDiaSymbol* _global_scope;
    IDiaDataSource* _source;
    IDiaSession* _session;
};

}