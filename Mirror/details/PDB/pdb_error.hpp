#include "../../types.hpp"

#include <winerror.h>

namespace meta::details
{

class pdb_error : public reflection_error
{
public:
    pdb_error(const std::string&, HRESULT);

private:
    std::string getComErrorMessage(HRESULT hresult);
};

}