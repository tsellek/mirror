#include "Pdb.hpp"
#include "Symbol.hpp"

#include "pdb_error.hpp"

#include "Callback.h"

namespace meta::details
{

Pdb::Pdb(const std::wstring& path)
{
    HRESULT hr = CoInitialize(NULL);
    if (FAILED(hr))
    {
        throw pdb_error("Failed to initialize COM", hr );
    }
    
    hr = CoCreateInstance( CLSID_DiaSource,
                        NULL,
                        CLSCTX_INPROC_SERVER,
                        __uuidof( IDiaDataSource ),
                        (void **) &_source);

    if (FAILED(hr))
    {
        throw pdb_error("Can't create DIA data source. Is msdia80.dll/msdia100.dll/msdia140.dll registered?", hr );
    }


    CCallback cb;
    cb.AddRef();
    hr = _source->loadDataForExe(path.c_str(), NULL, &cb);
    if (FAILED(hr))
    {
        throw pdb_error("Can't load debug information", hr );
    }

    hr = _source->openSession(&_session);
    if (FAILED(hr))
    {
        throw pdb_error("Can't load debug information", hr );
    }

    hr = _session->get_globalScope(&_global_scope);
    if (FAILED(hr))
    {
        throw pdb_error("Can't load debug information", hr );
    }
}


Pdb::~Pdb()
{
    _global_scope->Release();
    _session->Release();
    _source->Release();
    CoUninitialize();
}

std::vector<Symbol> Pdb::GlobalTypes()
{
    return findChildren(_global_scope, SymTagUDT);
}

Symbol Pdb::SymbolById(DWORD id)
{
    return Symbol{DiaSymbolById(id)};
}

std::vector<Symbol> Pdb::Methods(DWORD id)
{
    IDiaSymbol* sym = DiaSymbolById(id);

    return findChildren(sym, SymTagFunction);
}

std::vector<Symbol> Pdb::Arguments(DWORD id)
{
    IDiaSymbol* sym = DiaSymbolById(id);

    return findChildren(sym, SymTagFunctionArgType);
}

std::vector<Symbol> Pdb::Members(DWORD id)
{
    IDiaSymbol* sym = DiaSymbolById(id);

    return findChildren(sym, SymTagData);
}

std::vector<Symbol> Pdb::Children(DWORD id)
{
    IDiaSymbol* sym = DiaSymbolById(id);
    return findChildren(sym, SymTagNull);
}

IDiaSymbol* Pdb::DiaSymbolById(DWORD id)
{
    IDiaSymbol* diasym;
    HRESULT hr = _session->symbolById(id, &diasym);
    if (FAILED(hr))
    {
        throw pdb_error("Can't load symbol", hr );
    }
    return diasym;
}

std::vector<Symbol> Pdb::findChildren(IDiaSymbol* sym, enum SymTagEnum kind)
{
    std::vector<Symbol> ret;

    IDiaEnumSymbols *children;
    HRESULT hr = sym->findChildren(kind, NULL, nsNone, &children);
    if (FAILED(hr))
    {
        throw pdb_error("Can't load debug information", hr );
    }

    IDiaSymbol *pSymbol;
    ULONG celt = 0;

    while (SUCCEEDED(children->Next(1, &pSymbol, &celt)) && (celt == 1)) {        
        ret.emplace_back(pSymbol);
    }
/*
    LONG nchildren = 0;
    hr = children->get_Count(&nchildren);
    if (FAILED(hr))
    {
        throw pdb_error("Can't load debug information", hr );
    }

    if (nchildren <= 0)
    {
        throw std::runtime_error("Sub-types not found");
    }

    for (DWORD i=0; i < (DWORD)nchildren; ++i)
    {
        IDiaSymbol* child;
        hr = children->Item(i, &child);
        if (FAILED(hr))
        {
            throw pdb_error("Can't load child element", hr );
        }
        ret.emplace_back(child);
    }
*/
    return ret;
}

}