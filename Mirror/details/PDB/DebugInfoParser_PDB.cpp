#include "DebugInfoParser_PDB.hpp"

#include "Symbol.hpp"
#include "Pdb.hpp"

#include <boost/algorithm/string/predicate.hpp>

#include <iostream>
#include <map>

namespace meta::details
{

struct DebugInfoParser_PDB::Impl{
    Pdb pdb;

    std::map<std::string, DWORD> infoids;

    uint64_t base_address = 0;

    Impl(const std::wstring& path)
    :pdb(path)
    {}
};

DebugInfoParser_PDB::DebugInfoParser_PDB(const std::string& path, uint64_t baseaddr)
:_pImpl(new Impl(std::wstring{path.begin(), path.end()}), &DeleteImpl)
{
    _pImpl->base_address = baseaddr;
    loadClassTypesAtGlobalScope();
}

DebugInfoParser_PDB::~DebugInfoParser_PDB()
{

}

uint64_t DebugInfoParser_PDB::getClassDbgInfoId(const std::string& type_name, bool) 
{
    if (type_name.starts_with("class "))
    {
        return _pImpl->infoids.at(type_name.substr(6));
    }

    throw reflection_error("Only class types are supported");
}


void DebugInfoParser_PDB::loadClassTypesAtGlobalScope()
{
    std::vector<Symbol> globaltypes = _pImpl->pdb.GlobalTypes();

    for (Symbol type:globaltypes)
    {
        if (type.CompoundTypeKind() == CompoundTypeKind::Class)
        {
            std::string name = type.Name();
            if (boost::starts_with(name, "std::")||
                boost::starts_with(name, "boost::"))
            {
                continue;
            }
            if (_pImpl->infoids.find(name) == _pImpl->infoids.end())
            {
                _pImpl->infoids.insert(std::make_pair(type.Name(), type.Id()));
                std::clog<<name<<"["<<type.Id()<<"]"<<std::endl;
            }
        }
    }
    std::clog<<"----"<<std::endl;
}

unsigned DebugInfoParser_PDB::DbgInfoIdToSymbolIdx(uint64_t dbginfoid)
{
    return (DWORD)dbginfoid;
}

std::string DebugInfoParser_PDB::Name(uint64_t dbginfoid)
{
    return _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid)).Name();
}

size_t DebugInfoParser_PDB::Size(uint64_t dbginfoid)
{
    Symbol sym = _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid));
    size_t size = sym.Size();

    if (size == 0)
    {
        Symbol typesym = _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(sym.GetType()));
        return typesym.Size();
    }

    return size;
}

Accessibility DebugInfoParser_PDB::Access(uint64_t dbginfoid)
{
    return _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid)).Access();
}

std::vector<uint64_t> DebugInfoParser_PDB::Methods(uint64_t dbginfoid)
{    
    std::vector<Symbol> syms = _pImpl->pdb.Methods(DbgInfoIdToSymbolIdx(dbginfoid));
    std::vector<uint64_t> ret;
    std::transform(syms.begin(), syms.end(), std::back_inserter(ret), [](const Symbol& s){ return s.Id();});
    return ret;
}

std::vector<uint64_t> DebugInfoParser_PDB::Members(uint64_t dbginfoid)
{
    std::vector<Symbol> syms = _pImpl->pdb.Members(DbgInfoIdToSymbolIdx(dbginfoid));
    std::vector<uint64_t> ret;
    std::transform(syms.begin(), syms.end(), std::back_inserter(ret), [](const Symbol& s){ return s.Id();});
    return ret;
}


size_t DebugInfoParser_PDB::MemberOffset(uint64_t dbginfoid)
{
    return _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid)).MemberOffset();
}

uint64_t DebugInfoParser_PDB::TypeOf(uint64_t dbginfoid)
{
    Symbol s = _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid));

    if (s.IsSubroutine())
    {
        return TypeOf(s.GetType());
    }

    return s.GetType();
}

bool DebugInfoParser_PDB::TypeIsIndirection(uint64_t dbginfoid)
{
    return _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid)).IsIndirection();
}

bool DebugInfoParser_PDB::TypeIsSubroutine(uint64_t dbginfoid)
{
    return _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid)).IsSubroutine();
}

uint64_t DebugInfoParser_PDB::SubroutineEntry(uint64_t dbginfoid)
{
    DWORD rva = _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid)).RelativeVA();
    std::clog<<std::hex<<"Relative address of method: 0x"<<rva<<std::endl;
    return rva + _pImpl->base_address;
}

bool DebugInfoParser_PDB::TypeIsCv(uint64_t dbginfoid)
{
    return _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid)).IsCV();
}

std::vector<uint64_t> DebugInfoParser_PDB::Arguments(uint64_t dbginfoid)
{
    Symbol s = _pImpl->pdb.SymbolById(DbgInfoIdToSymbolIdx(dbginfoid));
    DWORD functype = DbgInfoIdToSymbolIdx(s.GetType());
    std::vector<Symbol> syms = _pImpl->pdb.Arguments(functype);
    std::vector<uint64_t> ret;
    std::transform(syms.begin(), syms.end(), std::back_inserter(ret), [](const Symbol& s){ return s.Id();});
    return ret;
}

bool DebugInfoParser_PDB::CvTypeUnderlyingTypeIsSeparate()
{
    return false;
}

void DebugInfoParser_PDB::DeleteImpl(Impl* p)
{
    delete p;
}

}