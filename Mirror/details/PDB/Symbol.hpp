#pragma once

#include "../../types.hpp"

#include <Dia2.h>

#include <string>

namespace meta::details
{

enum class CompoundTypeKind
{
    Union,
    Struct,
    Class,
    Unknown
};

class Symbol
{
public:
    Symbol(IDiaSymbol* sym);

    DWORD Id() const;
    std::string Name() const;
    uint64_t Size() const;
    uint64_t GetType() const;
    Accessibility Access() const;
    DWORD Tag() const;

    CompoundTypeKind CompoundTypeKind() const;

    bool IsSubroutine() const;
    bool IsCV() const;
    bool IsIndirection() const;

    SourceLocation SourceLocation() const;

    LONG MemberOffset() const; 

    DWORD RelativeVA() const;

private:
    std::string BaseTypeName() const;
    std::string IndirectionTypeName() const;

private:
    IDiaSymbol* _sym;
    std::string _name;
    DWORD _tag;
};

}