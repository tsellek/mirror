#include "pdb_error.hpp"

#include <comdef.h>

namespace meta::details
{

pdb_error::pdb_error(const std::string& what, HRESULT hresult)
:reflection_error(what + getComErrorMessage(hresult))
{

}

std::string pdb_error::getComErrorMessage(HRESULT hresult)
{
    _com_error err(hresult);
    return err.ErrorMessage();
}

}