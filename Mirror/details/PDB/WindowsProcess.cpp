#include "WindowsProcess.hpp"

#include "../../types.hpp"

#include <Windows.h>
#include <PSAPI.h>
#include <filesystem>

namespace meta::details
{
    WindowsProcess::WindowsProcess()
    {

    }

    void WindowsProcess::loadModules()
    {
        HMODULE modules[256];
        DWORD reqd_space;
        BOOL ok = EnumProcessModulesEx(GetCurrentProcess(), modules, sizeof(modules), &reqd_space, LIST_MODULES_DEFAULT);

        if (!ok)
        {
            throw reflection_error("Failed to enumerate modules");
        }

        unsigned nmodules = reqd_space/sizeof(HMODULE);

        for (unsigned module_idx = 0; module_idx < nmodules; ++module_idx)
        {
            char modname[MAX_PATH];
            ok = GetModuleFileNameEx(GetCurrentProcess(), modules[module_idx], modname, sizeof(modname));
            if (!ok)
            {
                throw reflection_error("Failed to get module name");
            }

            //per https://docs.microsoft.com/en-us/windows/desktop/api/psapi/ns-psapi-_moduleinfo
            //the module base address is the same as the HMODULE value
            _modules.push_back({modname, (uint64_t)modules[module_idx]});
        }

    }
}