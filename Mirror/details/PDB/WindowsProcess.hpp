#include "../Process.hpp"

namespace meta::details
{

class WindowsProcess : public Process
{
public:
    WindowsProcess();
protected:
    virtual void loadModules() override;
};

}