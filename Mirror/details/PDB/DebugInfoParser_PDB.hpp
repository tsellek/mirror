#pragma once

#include "../DebugInfoParser.hpp"

namespace meta::details
{

class DebugInfoParser_PDB : public DebugInfoParser
{
public:
    DebugInfoParser_PDB(const std::string&, uint64_t baseaddr);
    ~DebugInfoParser_PDB();

    virtual uint64_t getClassDbgInfoId(const std::string&, bool name_is_mangled) override;

private:
    void loadClassTypesAtGlobalScope();
    unsigned DbgInfoIdToSymbolIdx(uint64_t);

///from DebugDataFetcher
    virtual std::string Name(uint64_t dbginfoid) override;
    virtual size_t Size(uint64_t dbginfoid) override;
    virtual Accessibility Access(uint64_t dbginfoid) override;
    virtual std::vector<uint64_t> Methods(uint64_t dbginfoid) override;
    virtual std::vector<uint64_t> Members(uint64_t dbginfoid) override;
    virtual size_t MemberOffset(uint64_t dbginfoid) override;
    virtual uint64_t TypeOf(uint64_t dbginfoid) override;

    virtual bool TypeIsIndirection(uint64_t dbginfoid) override;
    virtual bool TypeIsSubroutine(uint64_t dbginfoid) override;
    virtual uint64_t SubroutineEntry(uint64_t dbginfoid) override;
    virtual bool TypeIsCv(uint64_t dbginfoid) override;

    virtual std::vector<uint64_t> Arguments(uint64_t dbginfoid) override;

    virtual bool CvTypeUnderlyingTypeIsSeparate() override;

private:
    struct Impl;
    static void DeleteImpl(Impl*);
    std::unique_ptr<Impl, decltype(&DeleteImpl)> _pImpl;
};

}