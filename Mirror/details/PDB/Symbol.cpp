#include "Symbol.hpp"

#include <string>
#include "pdb_error.hpp"

#include <iostream>
#include <sstream>

namespace meta::details
{

Symbol::Symbol(IDiaSymbol* sym)
:_sym(sym)
{
    _name = Name();
    _tag = Tag();
}

DWORD Symbol::Id() const
{
    DWORD id;
    HRESULT hr = _sym->get_symIndexId(&id);
    if (FAILED(hr))
    {
        throw pdb_error("Can't get symbol ID", hr );
    }

    return id;
}

std::string Symbol::Name() const
{
    BSTR name;
    HRESULT hr = _sym->get_name(&name);
    if (hr == S_FALSE)
    {
        if (IsIndirection())
        {
            return IndirectionTypeName();
        }

        try
        {
            return BaseTypeName();
        }
        catch(const std::exception&)
        {
        }
        
        return "";
    }
    if (FAILED(hr))
    {
        throw pdb_error("Can't get symbol name", hr );
    }

    UINT namelen = SysStringLen(name);
    return std::string{name, name+namelen};
}

uint64_t Symbol::Size() const
{
    ULONGLONG size;
    HRESULT hr = _sym->get_length(&size);
    if (FAILED(hr))
    {
        throw pdb_error("Can't get symbol name", hr );
    }
    return size;
}

uint64_t Symbol::GetType() const
{
    DWORD id;
    HRESULT hr = _sym->get_typeId(&id);
    if (hr == S_FALSE)
    {
        throw reflection_error("Symbol has no type");
    }
    if (FAILED(hr))
    {
        throw pdb_error("Can't get symbol type", hr );
    }
    return id;
}

Accessibility Symbol::Access() const
{
    DWORD access;
    HRESULT hr = _sym->get_access(&access);
    if (hr == S_FALSE)
    {
        std::clog<<"No accessibility for "<<Name()<<std::endl;
    }
    if (FAILED(hr))
    {
        throw pdb_error("Can't get symbol access", hr );
    }

    switch (access)
    {
    case CV_private:
        return Accessibility::Private;
    case CV_protected:
        return Accessibility::Protected;
    case CV_public:
        return Accessibility::Public;
    };

    return Accessibility::Unknown;
}

DWORD Symbol::Tag() const
{
    DWORD tag;
    HRESULT hr = _sym->get_symTag(&tag);
    if (hr == S_FALSE)
    {
        return 0;
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get symbol tag", hr );
    }
    return tag;
}

CompoundTypeKind Symbol::CompoundTypeKind() const
{
    DWORD udtkind;
    HRESULT hr = _sym->get_udtKind(&udtkind);

    if (hr == S_FALSE)
    {
        throw reflection_error("Not a compound type");
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get compound type kind", hr );
    }

    switch(udtkind)
    {
    case UdtStruct:
        return CompoundTypeKind::Struct;
    case UdtClass:
        return CompoundTypeKind::Class;
    case UdtUnion:
        return CompoundTypeKind::Union;
    default:
        return CompoundTypeKind::Unknown;
    };
}

bool Symbol::IsSubroutine() const
{
    DWORD tag;
    HRESULT hr = _sym->get_symTag(&tag);
    if (hr == S_FALSE)
    {
        return false;
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get symbol tag", hr );
    }

    return (tag == SymTagFunction);
}

bool Symbol::IsCV() const
{
    BOOL cnst;
    HRESULT hr = _sym->get_constType(&cnst);
    if (hr == S_FALSE)
    {
        return false;
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get symbol tag", hr );
    }

    return cnst;
}

bool Symbol::IsIndirection() const
{
    return Tag() == SymTagPointerType;
}

SourceLocation Symbol::SourceLocation() const
{
    IDiaLineNumber* lineinfo;
    HRESULT hr = _sym->getSrcLineOnTypeDefn(&lineinfo);
    if (hr == S_FALSE)
    {
        throw reflection_error("No source line information");
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get source line", hr );
    }

    IDiaSourceFile* fileinfo;
    hr = lineinfo->get_sourceFile(&fileinfo);
    if (hr == S_FALSE)
    {
        throw reflection_error("No source file information");
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get source file", hr );
    }

    DWORD line;
    hr = lineinfo->get_lineNumber(&line);
    if (hr == S_FALSE)
    {
        throw reflection_error("No line number information");
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get source line number", hr );
    }

    BSTR filename;
    hr = fileinfo->get_fileName(&filename);
    if (hr == S_FALSE)
    {
        throw reflection_error("No file name information");
    }
    if (FAILED(hr))
    {
        throw pdb_error("Failed to get source file name", hr );
    }

    fileinfo->Release();
    lineinfo->Release();
    return meta::SourceLocation{std::string{filename, filename+SysStringLen(filename)}, line};
}

LONG Symbol::MemberOffset() const
{
    DWORD loctype;
    HRESULT hr = _sym->get_locationType(&loctype);
    if ((hr == S_FALSE) || 
        (FAILED(hr)))
    {
        throw reflection_error("Symbol has no location type. Was it optimized out?");
    }

    if (loctype != LocIsThisRel)
    {
        throw reflection_error("Unsupported symbol location (only non-static member variables in memory are supported)");
    }

    LONG offset;
    hr = _sym->get_offset(&offset);
    if ((hr == S_FALSE) || 
        (FAILED(hr)))
    {
        throw reflection_error("Symbol has no offset information");
    }

    return offset;
}

DWORD Symbol::RelativeVA() const
{
    DWORD rva;

    HRESULT hr = _sym->get_relativeVirtualAddress(&rva);
    if ((hr == S_FALSE) || 
        (FAILED(hr)))
    {
        throw reflection_error("Symbol has no address information. Are you trying to call a function that was inlined?");
    }

    return rva;
}

std::string Symbol::BaseTypeName() const
{
    DWORD basetype;
    HRESULT hr = _sym->get_baseType(&basetype);
    if (hr == S_FALSE)
    {
        throw reflection_error("Not a base type");
    }
    else if (FAILED(hr))
    {
        throw pdb_error("Failed to get base type information", hr);
    }

    std::stringstream name;

    switch (basetype)
    {
        case btFloat:
            switch (Size())
            {
            case 4:
                name<<"float";
            break;
            case 8:
                name<<"double";
                break;
            case 10:
                name<<"long double";
                break;
            default:
                throw reflection_error("Unrecognized float size");
            }
            break;
        case btUInt:
            name<<"unsigned ";
            [[fallthrough]];
        case btInt:
            switch (Size())
            {
            case 2:
                name<<"short ";
                break;
            case 4:
                break;
            case 8:
                name<<"long long ";
                break;
            default:
                throw reflection_error("Unrecognized int size");
            }
            name<<"int";
            break;
        case btWChar:
            name<<"wchar_t";
            break;
        case btChar:
            name<<"char";
            break;
        case btBool:
            name<<"bool";
            break;
        case btVoid:
            name<<"void";
            break;
        default:
            throw reflection_error("Unrecognized type");
    };

    return name.str();
}

std::string Symbol::IndirectionTypeName() const
{
    BOOL ret;
    HRESULT hr = _sym->get_reference(&ret);
    if (hr == S_FALSE)
    {
        throw reflection_error("Not a pointer type");
    }
    else if (FAILED(hr))
    {
        throw pdb_error("Failed to get pointer type information", hr);
    }

    if (ret)
    {
        return "&";
    }
    else
    {
        return "*";
    }


}

}