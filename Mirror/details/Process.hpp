#pragma once

#include <vector>
#include <string>
#include <cstdint>

struct Module
{
    std::string Name;
    uint64_t BaseAddress;
};

class Process
{
protected:
    typedef std::vector<Module> modules_t;

public:
    typedef modules_t::const_iterator modules_iterator;

public:
    Process();
    virtual ~Process();

    modules_iterator modules_begin();
    modules_iterator modules_end();

protected:
    virtual void loadModules() = 0;

protected:
    modules_t _modules;

};