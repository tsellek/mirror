#pragma once

#include "../Type.hpp"

#include <type_traits>

namespace meta::details
{

template<typename IterType, typename CurArgType, typename... Args>
std::enable_if_t<std::is_reference_v<CurArgType> || std::is_pointer_v<CurArgType>, void> 
VerifyMethodCallArgs_Impl(IterType expected_arg_it);


template<typename IterType, typename CurArgType, typename... Args>
std::enable_if_t<!std::is_reference_v<CurArgType> && !std::is_pointer_v<CurArgType>, void> 
VerifyMethodCallArgs_Impl(IterType expected_arg_it);

template<typename IterType>
void VerifyMethodCallArgs_Impl(IterType /*expected_arg_it*/)
{
}

template<typename IterType, typename CurArgType, typename... Args>
std::enable_if_t<std::is_reference_v<CurArgType> || std::is_pointer_v<CurArgType>, void> 
VerifyMethodCallArgs_Impl(IterType expected_arg_it)
{
    if(expected_arg_it->GetType().Size() != sizeof(void*))
    {
        throw reflection_error("Incompatible argument " + expected_arg_it->GetType().Name() +" "+ expected_arg_it->Name());
    }

    if (sizeof...(Args) > 0)
    {
        VerifyMethodCallArgs_Impl<IterType, Args&&...>(expected_arg_it + 1);
    }
}

template<typename IterType, typename CurArgType, typename... Args>
std::enable_if_t<!std::is_reference_v<CurArgType> && !std::is_pointer_v<CurArgType>, void> 
VerifyMethodCallArgs_Impl(IterType expected_arg_it)
{
    if(sizeof(CurArgType) != expected_arg_it->GetType().Size())
    {
        throw reflection_error("Incompatible argument " + expected_arg_it->GetType().Name() +" "+ expected_arg_it->Name());
    }

    if (sizeof...(Args) > 0)
    {
        VerifyMethodCallArgs_Impl<IterType, Args&&...>(expected_arg_it + 1);
    }
}

template<typename IterType, typename... Args>
void VerifyMethodCallArgs(IterType expected_arg_it)
{
    VerifyMethodCallArgs_Impl<IterType, Args...>(expected_arg_it);
}

template<typename RetType, typename IterType, typename... Args>
typename std::enable_if<!std::is_void_v<RetType>, void>::type 
VerifyMethodCall(Type expected_ret_type, IterType expected_args_begin, IterType expected_args_end, Args... args)
{
    if (sizeof...(args) != std::distance(expected_args_begin, expected_args_end))
    {
        throw reflection_error("Invalid number of arguments");
    }
    if (sizeof(RetType) != expected_ret_type.Size())
    {
        throw reflection_error("Incompatible return type");
    }
    VerifyMethodCallArgs<IterType, Args...>(expected_args_begin);
}

template<typename RetType, typename IterType, typename... Args>
typename std::enable_if<std::is_void_v<RetType>, void>::type 
VerifyMethodCall(Type expected_ret_type, IterType expected_args_begin, IterType expected_args_end, Args... args)
{
    if (sizeof...(args) != std::distance(expected_args_begin, expected_args_end))
    {
        throw reflection_error("Invalid number of arguments");
    }
    if (expected_ret_type.Name() != "void")
    {
        throw reflection_error("Using void as return type of a method returning non-void");
    }
    VerifyMethodCallArgs<IterType, Args...>(expected_args_begin);
}

}