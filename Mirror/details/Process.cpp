#include "Process.hpp"

Process::Process()
{

}

Process::~Process()
{

}

Process::modules_iterator Process::modules_begin()
{
    if (_modules.empty())
    {
        loadModules();
    }
    return _modules.cbegin();
}

Process::modules_iterator Process::modules_end()
{
    return _modules.cend();
}