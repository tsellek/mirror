#include "Mirror.hpp"

#include "Process.hpp"

#if defined(__linux__ )
#include "DWARF/DebugInfoParser_DWARF.hpp"
#include "DWARF/LinuxProcess.hpp"
#define DEBUG_PARSER_TYPE DebugInfoParser_DWARF
#define PROCESS_TYPE LinuxProcess
#elif defined(_WIN32)
#include "PDB/DebugInfoParser_PDB.hpp"
#include "PDB/WindowsProcess.hpp"
#define DEBUG_PARSER_TYPE DebugInfoParser_PDB
#define PROCESS_TYPE WindowsProcess
#endif

using namespace std::string_literals;

namespace meta::details
{
    struct Mirror::Impl
    {
        std::vector<std::shared_ptr<DebugInfoParser>> parsers;
        std::unique_ptr<Process> cur_process;

        Impl()
        :cur_process(new PROCESS_TYPE)
        {}
    };

    Mirror& Mirror::Instance()
    {
        static Mirror theMirror;
        return theMirror;
    }

    Mirror::Mirror()
    :_pImpl(new Impl, &DeleteImpl)
    {

    }

    Class Mirror::MetaclassFor(const std::string& name, bool name_is_mangled)
    {
        return MetaclassFor(name, nullptr, name_is_mangled);
    }

    Class Mirror::MetaclassFor(const std::string& name, void* This, bool name_is_mangled)
    {
        if (_pImpl->parsers.empty())
        {
            LoadModules();
        }

        for (auto& p: _pImpl->parsers)
        {
            try
            {
                return Class(This, p->getClassDbgInfoId(name, name_is_mangled), p.get() );
            }
            catch(const reflection_error&)
            {

            }
        }
        throw reflection_error("Type information for type "s+std::string(name)+" not found"s);
    }

    Type Mirror::TypeFor(const std::type_info& info)
    {
        if (_pImpl->parsers.empty())
        {
            LoadModules();
        }

        for (auto& p: _pImpl->parsers)
        {
            try
            {
                return Type(p->getClassDbgInfoId(info.name()), p.get() );
            }
            catch(const reflection_error&)
            {

            }
        }
        throw reflection_error("Type information for type "s+info.name()+" not found"s);
    }

    Variable Mirror::VariableFor(const std::type_info& info, void* This)
    {
        if (_pImpl->parsers.empty())
        {
            LoadModules();
        }

        for (auto& p: _pImpl->parsers)
        {
            try
            {
                return Variable(This, p->getClassDbgInfoId(info.name()), p.get() );
            }
            catch(const reflection_error&)
            {

            }
        }
        throw reflection_error("Type information for type "s+info.name()+" not found"s);
    }

    void Mirror::LoadModules()
    {
        for (auto   m = _pImpl->cur_process->modules_begin(); 
                    m != _pImpl->cur_process->modules_end();
                    ++m)
        {
            try
            {
                _pImpl->parsers.emplace_back(new DEBUG_PARSER_TYPE(m->Name, m->BaseAddress));
            }
            catch(const reflection_error&)
            {
            }
        }

        if (_pImpl->cur_process->modules_begin() == _pImpl->cur_process->modules_end())
        {
            throw reflection_error("no debug information found for any of the loaded modules");
        }
    }

    void Mirror::DeleteImpl(Impl* p)
    {
        delete p;
    }

    Class meta_cast_class_by_name(const std::string& type_name)
    {
        return Mirror::Instance().MetaclassFor(type_name, false);
    }
}