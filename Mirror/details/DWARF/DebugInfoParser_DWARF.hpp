#include "../DebugInfoParser.hpp"

#include <string>
#include <memory>

namespace meta::details
{

class Die;

class DebugInfoParser_DWARF : public DebugInfoParser{
public:
    DebugInfoParser_DWARF(const std::string& path, uint64_t base_address);

    virtual uint64_t getClassDbgInfoId(const std::string& class_name, bool name_is_mangled) override;

private:    
    uint64_t DbgInfoIdFor(const std::string& type_name);

    void loadGlobalClassTypes();
    void loadClassTypesRecursively(Die first_sibling, const std::string& namespace_prefix);
    // void loadClassMethodDeclerations(Die);
    void loadFunctionDefinitions();

    bool NextCu();

    Die DbgInfoIdToDie(uint64_t);
    uint64_t DieToDbgInfoId(Die);

    bool UseUnderlyingType(Die);
    Die GetUnderlyingTypeDie(Die);

    void LoadClassInfo(Die, const std::string& name_prefix);
    void LoadFundamentalTypeInfo(Die, const std::string& name_prefix);

    std::string DemangledName(const std::string&);
    std::string FullName(const std::string& namespace_name, const std::string& type_name);

private: //inherited from DebugInfoFetcher
    virtual std::string Name(uint64_t dbginfoid) override;
    virtual size_t Size(uint64_t dbginfoid) override;
    virtual Accessibility Access(uint64_t dbginfoid) override;
    virtual std::vector<uint64_t> Methods(uint64_t dbginfoid) override;
    virtual std::vector<uint64_t> Members(uint64_t dbginfoid) override;
    virtual size_t MemberOffset(uint64_t dbginfoid) override;
    virtual uint64_t TypeOf(uint64_t dbginfoid) override;

    virtual bool TypeIsSubroutine(uint64_t dbginfoid) override;
    virtual uint64_t SubroutineEntry(uint64_t dbginfoid) override;
    virtual TypeTraits Traits(uint64_t dbginfoid) override;

    virtual std::vector<uint64_t> Arguments(uint64_t dbginfoid) override;

private:
    struct Impl;

    static void DeleteImpl(Impl*); 
    std::unique_ptr<Impl, decltype(&DeleteImpl)> _pImpl;
};
}