#include "Dwarf.hpp"
#include "dwarf_error.hpp"

#include <iostream>

#include <sys/types.h> /* For open() */
#include <sys/stat.h>  /* For open() */
#include <fcntl.h>     /* For open() */
#include <unistd.h>     /* For close() */

namespace meta::details
{

Dwarf::Dwarf(const std::string& path){
    
    _fd = open(path.c_str(),O_RDONLY);

    Dwarf_Error err = 0;

    int res = dwarf_init_b(_fd, DW_GROUPNUMBER_ANY, NULL, NULL, &_dbg, &err);
    if(res != DW_DLV_OK) {
        throw dwarf_error("Failed to initialize", nullptr, err);
    }

}


bool Dwarf::NextCompilationUnit(){
    Dwarf_Error error;
    Dwarf_Unsigned cu_next_offset;

    int res = dwarf_next_cu_header_d(   _dbg, 1, 
                                        NULL, NULL, 
                                        NULL, NULL,
                                        NULL, NULL,
                                        NULL, NULL, 
                                        &cu_next_offset, NULL, &error);


    if(res == DW_DLV_ERROR) {
        throw dwarf_error("Error in dwarf_next_cu_header", _dbg, error);
    }

    if(res == DW_DLV_NO_ENTRY) {
        return false;
    }

    return true;

}

std::optional<Die> Dwarf::FirstDieInCompilationUnit()
{
    Dwarf_Die first_die;
    Dwarf_Error error;
    int res = dwarf_siblingof_b(_dbg, NULL, 1, &first_die, &error);
    if(res != DW_DLV_OK) {
        throw dwarf_error("Failed to get initial DIE", _dbg, error);
    }

    return Die(_dbg, make_dwarf_ptr(_dbg, first_die));
}

std::optional<Die> Dwarf::DieByOffset(uint64_t offset)
{
    Dwarf_Die d;
    Dwarf_Error err;

    int res = dwarf_offdie_b(_dbg, offset, 1, &d, &err);

    if(res != DW_DLV_OK) {
        std::clog<<"Dwarf::DieByOffset("<<offset<<") - "<<dwarf_errmsg(err)<<std::endl;
        return std::optional<Die>();
    }

    return Die(_dbg, make_dwarf_ptr(_dbg, d));

}

Dwarf::~Dwarf(){
    
    dwarf_finish(_dbg);
    close(_fd);
}

}