#include "Die.hpp"

#include "dwarf_error.hpp"

#include <dwarf.h>

#include <iostream>

using namespace std::string_literals;

namespace meta::details
{

template<typename T>
T GetDwarfAttr(Dwarf_Debug dbg, Dwarf_Die die, Dwarf_Half form, unsigned attr_id, int(*getterfunc)(Dwarf_Attribute, T*, Dwarf_Error*) )
{
    Dwarf_Attribute attr;
    Dwarf_Error err;
    Dwarf_Bool hasattr;

    int res = dwarf_hasattr(die, attr_id, &hasattr, &err);
    if (res != DW_DLV_OK){
        throw dwarf_error("Failed to query for attribute", dbg, err);
    }
    if (!hasattr)
    {
        throw reflection_error("Attribute not found");
    }

    res = dwarf_attr(die, attr_id,&attr, &err);
    if (res != DW_DLV_OK){
        throw dwarf_error("Failed to get attribute", dbg, err);
    }

    Dwarf_Half actual_form;
    res = dwarf_whatform(attr, &actual_form, &err);
    if (res != DW_DLV_OK){
        throw dwarf_error("Failed to get attribute form", dbg, err);
    }

    if (form != actual_form){
        throw reflection_error("Unexpected attribute form");
    }

    T value;
    res = getterfunc(attr, &value, &err);
    if (res != DW_DLV_OK){
        throw dwarf_error("Failed to get decleration file index", dbg, err);
    }

    return value;
}

Die::Die(Dwarf_Debug dbg, dwarf_ptr<Dwarf_Die> die)
:_dbg(dbg), _die(die)
{
    Dwarf_Error err = 0;
    char* name;

    if (!_dbg)
    {
        throw reflection_error("No debug information");
    }

    int res = dwarf_tag(_die, &_tag, &err);
    if(res != DW_DLV_OK) {
        throw dwarf_error("Failed to get tag", _dbg, err);
    }

    bool load_data = true;
    switch(_tag)
    {
        case DW_TAG_class_type:
            _type = DieType::Class;
            break;        
        case DW_TAG_structure_type:
            _type = DieType::Struct;
            break;
        case DW_TAG_union_type:
            _type = DieType::Union;
            break;
        case DW_TAG_base_type:
        case DW_TAG_enumeration_type:
            _type = DieType::Type;
            break;
        case DW_TAG_typedef:
            _type = DieType::Typedef;
            break;
        case DW_TAG_subprogram:
            _type = DieType::Subprogram;
            break;
        case DW_TAG_member:
        case DW_TAG_formal_parameter:
            _type = DieType::Variable;
            break;
        case DW_TAG_compile_unit:
            _type = DieType::CompilationUnit;
            break;
        case DW_TAG_subroutine_type:
            _type = DieType::SubroutineType;
            break;
        case DW_TAG_pointer_type:
            _type = DieType::Pointer;
            break;
        case DW_TAG_reference_type: 
        case DW_TAG_rvalue_reference_type: 
            _type = DieType::Reference;
            break;
        case DW_TAG_const_type:
            _type = DieType::Const;
            break;
        case DW_TAG_volatile_type:
            _type = DieType::Voaltile;
            break;
        case DW_TAG_namespace:
            _type = DieType::Namespace;
            break;
        default:
            load_data = false;
            /*const char* tagname;
            char* name;
            dwarf_get_TAG_name(tag, &tagname);
            dwarf_diename(_die,&name,NULL);
            std::clog<<"Not loading data for DIE "<<name<<" with tag "<<tagname<<std::endl;

            dwarf_dealloc(_dbg, name, DW_DLA_STRING);*/
            
    }

    if (load_data)
    {
        res = dwarf_diename(_die,&name,&err);
        if(res == DW_DLV_ERROR) {
            throw dwarf_error("Failed to get name", _dbg, err);
        }
        else if (res == DW_DLV_NO_ENTRY) {
            _name = "";
        }
        else
        {
            _name = std::string(name);
            dwarf_dealloc(_dbg, name, DW_DLA_STRING);
        }
        

        res = dwarf_dieoffset(_die, &_offset, &err);
        //res = dwarf_die_CU_offset(_die, &_offset, &err);
        if(res != DW_DLV_OK) {
            throw dwarf_error(std::string("Failed to get offset of ")+_name, _dbg, err);
        }
    }
}

std::optional<Die> Die::FirstChild() const
{
    Dwarf_Die child;
    Dwarf_Error err = 0;
    int res = dwarf_child(_die,&child,&err);

    if(res == DW_DLV_ERROR) {
        throw dwarf_error("Failed to get child", _dbg, err);
    }
    else if (res == DW_DLV_NO_ENTRY) {
        return std::optional<Die>();
    }

    return Die(_dbg, make_dwarf_ptr(_dbg, child));
}

std::optional<Die> Die::Sibling() const
{
    Dwarf_Die sib;
    Dwarf_Error err = 0;
    int res = dwarf_siblingof_b(_dbg, _die, 1, &sib, &err);

    if(res == DW_DLV_ERROR) {
        throw dwarf_error("Failed to get sibling", _dbg, err);
    }
    else if (res == DW_DLV_NO_ENTRY) {
        return std::optional<Die>();
    }

    return Die(_dbg, make_dwarf_ptr(_dbg, sib));
}

DieType Die::Type() const{
    return _type;
}

std::string Die::Name() const{
    if (!_name.empty())
    {
        return _name;
    }

    if (_tag == DW_TAG_pointer_type)
    {
        return "*"s;
    }
    if(_tag == DW_TAG_reference_type)
    {
        return "&"s;
    }

    if(_tag == DW_TAG_rvalue_reference_type) 
    {
        return "&&"s;
    }

    if (_tag == DW_TAG_const_type)
    {
        return "const"s;
    }

    if (_tag == DW_TAG_volatile_type)
    {
        return "volatile"s;
    }

    return std::string{};
}

uint64_t Die::Offset() const{
    return _offset;
}

size_t Die::Size() const{
    Dwarf_Unsigned size;
    Dwarf_Error err;
    int res = dwarf_bytesize(_die, &size, &err);
    if (res == DW_DLV_ERROR){
        throw dwarf_error("Failed to get size", _dbg, err);
    }
    else if (res == DW_DLV_NO_ENTRY){
        throw reflection_error("Die has no size");
    }


    return size;
}

Accessibility Die::Access() const
{
    try
    {    
        Dwarf_Unsigned access = GetDwarfAttr<Dwarf_Unsigned>(_dbg, _die, DW_FORM_data1, DW_AT_accessibility, dwarf_formudata);

        switch (access){
            case DW_ACCESS_private:
                return Accessibility::Private;
            case DW_ACCESS_protected:
                return Accessibility::Protected;
            case DW_ACCESS_public:
                return Accessibility::Public;
            default:
                return Accessibility::Unknown;
        };
    }
    catch(const dwarf_error&)
    {
        throw;
    }
    catch(const reflection_error& e)
    {//private members may have no accessibility attribute.
        return Accessibility::Private;
    }
    
}

uint64_t Die::TypeDieOffset() const
{
    Dwarf_Off offset = GetDwarfAttr<Dwarf_Unsigned>(_dbg, _die, DW_FORM_ref4, DW_AT_type, dwarf_global_formref);

    return offset;
}

const SourceLocation Die::Location() const
{    

    Dwarf_Unsigned fileid = GetDwarfAttr<Dwarf_Unsigned>(_dbg, _die, DW_FORM_data1, DW_AT_decl_file, dwarf_formudata);
    Dwarf_Unsigned line = GetDwarfAttr<Dwarf_Unsigned>(_dbg, _die, DW_FORM_data1, DW_AT_decl_line, dwarf_formudata);

    Die cudie = CompilationUnitDie();

    Dwarf_Error err;
    int res;
    char** srcfiles;
    Dwarf_Signed nfiles;
    res = dwarf_srcfiles(cudie._die, &srcfiles, &nfiles, &err);
    if (res != DW_DLV_OK){
        throw dwarf_error("Failed to get CU file list", _dbg, err);
    }

    std::string filename(srcfiles[fileid-1]);
    dwarf_dealloc(_dbg, srcfiles, DW_DLA_STRING);


    SourceLocation sl{filename, line};

    return sl;
}

size_t Die::DataMemberOffset() const
{   
    return GetDwarfAttr<Dwarf_Unsigned>(_dbg, _die, DW_FORM_data1, DW_AT_data_member_location, dwarf_formudata);
}

uint64_t Die::SubroutineEntry() const
{
    return GetDwarfAttr<Dwarf_Addr>(_dbg, _die, DW_FORM_addr, DW_AT_low_pc, dwarf_formaddr);
}

uint64_t Die::SubroutineDeclOffset() const
{
    return GetDwarfAttr<Dwarf_Unsigned>(_dbg, _die, DW_FORM_ref4, DW_AT_specification, dwarf_global_formref);
}

Die Die::CompilationUnitDie() const
{
    Dwarf_Off offset;
    Dwarf_Error err;
    int res = dwarf_CU_dieoffset_given_die(_die, &offset, &err);
    if (res != DW_DLV_OK){
        throw dwarf_error("Failed to get CU DIE offset", _dbg, err);
    }

    Dwarf_Die cudie;
    res = dwarf_offdie_b(_dbg, offset, 1, &cudie, &err);

    if(res != DW_DLV_OK) {
        throw dwarf_error("Failed to get CU DIE", _dbg, err);
    }

    return Die(_dbg, make_dwarf_ptr(_dbg, cudie));

}

}