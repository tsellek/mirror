#include "DebugInfoParser_DWARF.hpp"
#include "Die.hpp"
#include "Dwarf.hpp"
#include "dwarf_error.hpp"
#include <algorithm>
#include <map>

#include <cxxabi.h>

using namespace std;

namespace meta::details
{

struct DebugInfoParser_DWARF::Impl
{   
    Dwarf dwarf;
    std::multimap<std::string, uint64_t> class_dbginfo_offset;
    std::map<uint64_t, uint64_t> function_decl_to_def;
    //std::multimap<SourceLocation, uint64_t> function_decls;

    int cur_cu = -1;

    uint64_t base_addr = 0;

    Impl(const string& path)
    :dwarf(path)
    {

    }
};

DebugInfoParser_DWARF::DebugInfoParser_DWARF(const string& path, uint64_t base_address)
:_pImpl(new Impl(path), &DeleteImpl)
{
    _pImpl->base_addr = base_address;
    loadFunctionDefinitions();
    loadGlobalClassTypes();
}

uint64_t DebugInfoParser_DWARF::getClassDbgInfoId(const std::string& class_name, bool name_is_mangled)
{
    const std::string demangled_name = name_is_mangled ? DemangledName(class_name) : class_name;
    return DbgInfoIdFor(demangled_name);    
}

void DebugInfoParser_DWARF::loadClassTypesRecursively(Die first_sibling, const std::string& namespace_prefix) 
{
    auto typedie = std::optional(first_sibling);
    while ((typedie = typedie->Sibling()))
    {
        if (typedie->Type() == DieType::Type)
        {
            LoadFundamentalTypeInfo(typedie.value(), namespace_prefix);
        }
        else if (typedie->Type() == DieType::Class)
        {                
            LoadClassInfo(typedie.value(), namespace_prefix);
        }
        else if (typedie->Type() == DieType::Namespace && typedie->Name() != "std"s && typedie->Name() != "__gnu_cxx")
        {
            if (typedie->FirstChild()) {
                loadClassTypesRecursively(typedie->FirstChild().value(), FullName(namespace_prefix, typedie->Name()));
            }
        }
    }
}

void DebugInfoParser_DWARF::loadGlobalClassTypes()
{
    while (NextCu())
    {
        auto cudie = _pImpl->dwarf.FirstDieInCompilationUnit();
        //std::string cu_name = cudie->Name();
        //std::clog<<_pImpl->cur_cu<<" ["<<cu_name<<"@0x"<<std::hex<<cudie->Offset()<<"]"<<std::endl;

        auto typedie = cudie->FirstChild();
        if (typedie) {
            loadClassTypesRecursively(typedie.value(), "");
        }
    }

}

// void DebugInfoParser_DWARF::loadClassMethodDeclerations(Die classdie)
// {
//     auto methoddie = classdie.FirstChild();

//     do
//     {
//         if (methoddie->Type() == DieType::Subprogram)
//         {
//             _pImpl->function_decls.insert(std::make_pair(methoddie->Location(), methoddie->Offset()));
//         }
//     }
//     while ((methoddie = methoddie->Sibling()));
// }

void DebugInfoParser_DWARF::loadFunctionDefinitions()
{
    while (NextCu())
    {
        auto cudie = _pImpl->dwarf.FirstDieInCompilationUnit();
        std::string cu_name = cudie->Name();
        //std::clog<<_pImpl->cur_cu<<" ["<<cu_name<<"@0x"<<std::hex<<cudie->Offset()<<"]"<<std::endl;

        auto subprog = cudie->FirstChild();

        while ((subprog = subprog->Sibling()))
        {
            if (subprog->Type() == DieType::Subprogram)
            {
                try
                {
                    uint64_t offset = subprog->Offset();
                    if (subprog->SubroutineEntry())
                    {
                        uint64_t decloffset = subprog->SubroutineDeclOffset();
                        _pImpl->function_decl_to_def[decloffset] = offset;
                        //std::cout<<std::hex<<decloffset<<"->"<<subprog->Offset()<<std::endl;
                    }
                    else
                    {
                    }
                }
                catch(const reflection_error&)
                {
                    //std::clog<<"No decl offset for "<<subprog->Name()<<std::endl;
                }
            }
        }
    }
}

bool DebugInfoParser_DWARF::NextCu()
{
    bool ret = _pImpl->dwarf.NextCompilationUnit();
    if (ret)
    {
        _pImpl->cur_cu++;
    }
    else
    {
        _pImpl->cur_cu = -1;
    }
    return ret;    
}

Die DebugInfoParser_DWARF::DbgInfoIdToDie(uint64_t dbginfoid)
{
    uint32_t offset = dbginfoid & 0xFFFFFFFF;
    
    auto die = _pImpl->dwarf.DieByOffset(offset);
    if (!die)
    {
        throw reflection_error("Invalid Debug Info ID");
    }

    return die.value();
}

uint64_t DebugInfoParser_DWARF::DieToDbgInfoId(Die d)
{
    return ((uint64_t)_pImpl->cur_cu<<32)|d.Offset();
}

bool DebugInfoParser_DWARF::UseUnderlyingType(Die d)
{
    return (d.Type() == DieType::Const) || (d.Type() == DieType::Voaltile);
}

Die DebugInfoParser_DWARF::GetUnderlyingTypeDie(Die d)
{
    auto ret = _pImpl->dwarf.DieByOffset(d.TypeDieOffset());

    if (!ret)
    {
        throw reflection_error("Expected underlying type not found");
    }
    return ret.value();
}

void DebugInfoParser_DWARF::LoadClassInfo(Die typedie, const std::string& name_prefix)
{
    try
    {
        typedie.Size(); //this will throw if the typedef does not have a size

        const auto full_name = FullName(name_prefix, typedie.Name());

        auto lower_bound = _pImpl->class_dbginfo_offset.lower_bound(full_name);
        auto upper_bound = _pImpl->class_dbginfo_offset.upper_bound(full_name);
        bool ambiguous = false;
        for (auto it = lower_bound; 
            it != upper_bound; 
            ++it)
        {
            if (typedie.Location() != DbgInfoIdToDie(it->second).Location())
            {
                ambiguous = true; 
            }
        }
        
        if (ambiguous || 
            (lower_bound == upper_bound))
        {
            uint64_t dbginfoid = DieToDbgInfoId(typedie);
            _pImpl->class_dbginfo_offset.insert(std::make_pair(full_name, dbginfoid));
/*                        std::clog<<"\t"<<typedie->Name()<<
                            "@0x"<<std::hex<<typedie->Offset()<<
                            " ["<<typedie->Location().file<<":"<<typedie->Location().line<<"]"<<
                            std::endl;                        */
        }

        // loadClassMethodDeclerations(typedie);

    }
    catch(const std::exception&)
    {
    }
}

void DebugInfoParser_DWARF::LoadFundamentalTypeInfo(Die typedie, const std::string& name_prefix)
{
    auto lower_bound = _pImpl->class_dbginfo_offset.lower_bound(typedie.Name());
    auto upper_bound = _pImpl->class_dbginfo_offset.upper_bound(typedie.Name());

    if (lower_bound == upper_bound)
    {
        //fundamental types are assumed to have the same meaning in all CUs
        _pImpl->class_dbginfo_offset.emplace(FullName(name_prefix, typedie.Name()), DieToDbgInfoId(typedie));
    }
}

std::string DebugInfoParser_DWARF::DemangledName(const std::string& mangled_name) {
    int status;
    char* nameptr = abi::__cxa_demangle(mangled_name.c_str(), nullptr, nullptr, &status);

    if (status < 0) {
        throw reflection_error("unable to demangle name "s + mangled_name);
    }
    const std::string ret(nameptr);
    free(nameptr);
    return ret;
}

std::string DebugInfoParser_DWARF::FullName(const std::string& namespace_name, const std::string& type_name) {
    return namespace_name.empty() ? type_name : namespace_name + "::" + type_name;
}

std::string DebugInfoParser_DWARF::Name(uint64_t dbginfoid)
{
    Die die = DbgInfoIdToDie(dbginfoid);
    if (UseUnderlyingType(die))
    {
        return GetUnderlyingTypeDie(die).Name();
    }
    return die.Name();
}

size_t DebugInfoParser_DWARF::Size(uint64_t dbginfoid)
{
    try
    {
        return DbgInfoIdToDie(dbginfoid).Size();
    }
    catch(const reflection_error& e)
    {
        Die underlyingtype = GetUnderlyingTypeDie(DbgInfoIdToDie(dbginfoid));
        return underlyingtype.Size();
    }
    
}

Accessibility DebugInfoParser_DWARF::Access(uint64_t dbginfoid)
{
    return DbgInfoIdToDie(dbginfoid).Access();
}

std::vector<uint64_t> DebugInfoParser_DWARF::Methods(uint64_t dbginfoid)
{
    auto die = DbgInfoIdToDie(dbginfoid).FirstChild();
    if (!die)
    {
        return std::vector<uint64_t>();
    }

    std::vector<uint64_t> ret;
    do
    {
        if (die->Type() == DieType::Subprogram)
        {
            uint64_t dbginfoid = DieToDbgInfoId(die.value());
            ret.push_back(dbginfoid);
        }
    }while ((die = die->Sibling()));

    return ret;
}

std::vector<uint64_t> DebugInfoParser_DWARF::Members(uint64_t dbginfoid)
{
    auto die = DbgInfoIdToDie(dbginfoid).FirstChild();
    if (!die)
    {
        return std::vector<uint64_t>();
    }

    std::vector<uint64_t> ret;
    do
    {
        if (die->Type() == DieType::Variable)
        {
            uint64_t dbginfoid = DieToDbgInfoId(die.value());
            ret.push_back(dbginfoid);
        }
    }while ((die = die->Sibling()));

    return ret;
}

size_t DebugInfoParser_DWARF::MemberOffset(uint64_t dbginfoid)
{
    Die die = DbgInfoIdToDie(dbginfoid);
    return die.DataMemberOffset();
}

uint64_t DebugInfoParser_DWARF::TypeOf(uint64_t dbginfoid)
{
    Die die = DbgInfoIdToDie(dbginfoid);
    if (UseUnderlyingType(die))
    {
        die = GetUnderlyingTypeDie(die);
    }
    uint64_t offset = die.TypeDieOffset();
    uint64_t cu_idx = dbginfoid >> 32;    
    uint64_t ret = (cu_idx<<32) | offset;
    return ret;
}

bool DebugInfoParser_DWARF::TypeIsSubroutine(uint64_t dbginfoid) 
{
    return DbgInfoIdToDie(dbginfoid).Type() == DieType::SubroutineType;
}

uint64_t DebugInfoParser_DWARF::SubroutineEntry(uint64_t dbginfoid)
{
    Die die = DbgInfoIdToDie(dbginfoid);
    try
    {
        return _pImpl->base_addr + die.SubroutineEntry();
    }
    catch(const reflection_error&)
    {
        const auto decl_offset = die.Offset();

        auto def_it = _pImpl->function_decl_to_def.find(decl_offset);
        if (def_it != _pImpl->function_decl_to_def.end())
        {
            uint64_t def_offset = def_it->second;
            uint64_t relative_addr = DbgInfoIdToDie(def_offset).SubroutineEntry();
            return (relative_addr + _pImpl->base_addr);
        }

        throw reflection_error("Function definition not found");

    }   
    
}

TypeTraits DebugInfoParser_DWARF::Traits(uint64_t dbginfoid)
{
    TypeTraits traits;

    auto die = DbgInfoIdToDie(dbginfoid);

    if (UseUnderlyingType(die))
    {
        Die underlyingtype = GetUnderlyingTypeDie(die);
        traits = Traits(DieToDbgInfoId(underlyingtype));
    }

    switch (die.Type())
    {
    case DieType::Const:
        traits.Const = true;
        break;
    case DieType::Voaltile:
        traits.Volatile = true;
        break;
    case DieType::Reference:
        traits.Reference = true;
        break;
    case DieType::Pointer:
        traits.Pointer = true;
        break;
    case DieType::Typedef:
        traits.Typedef = true;
    default:
    //do nothing
        break;
    }

    return traits;
}

std::vector<uint64_t> DebugInfoParser_DWARF::Arguments(uint64_t dbginfoid)
{
    auto die = DbgInfoIdToDie(dbginfoid).FirstChild();
    if (!die)
    {
        return std::vector<uint64_t>();
    }

    std::vector<uint64_t> ret;
    do
    {
        if (die->Type() == DieType::Variable)
        {
            uint64_t dbginfoid = DieToDbgInfoId(die.value());
            ret.push_back(dbginfoid);
        }
    }while ((die = die->Sibling()));

    return std::vector<uint64_t>{ret.begin()+1, ret.end()};
}

uint64_t DebugInfoParser_DWARF::DbgInfoIdFor(const std::string& type_name)
{
    const auto num_entries = _pImpl->class_dbginfo_offset.count(type_name);

    if (num_entries == 0)
    {
        const auto errmsg = "type not found "s+type_name;
        throw reflection_error(errmsg);
    }

    // if (num_entries > 1) {
    //     const auto errmsg = "ambiguous type "s+nameptr;
    //     free(nameptr);
    //     throw reflection_error(errmsg);
    // }

    const auto [first, last] = _pImpl->class_dbginfo_offset.equal_range(type_name);

    const auto chosen_dbginfo = std::max_element(first, last, [](const auto &it1, const auto &it2){return it1.first < it2.first;});
    return chosen_dbginfo->second;
}

void DebugInfoParser_DWARF::DeleteImpl(Impl* p)
{
    delete p;
}

}