#include "Die.hpp"

#include <libdwarf.h>

#include <string>
#include <optional>

namespace meta::details
{

class Dwarf{
public:
    Dwarf(const std::string& path);

    bool NextCompilationUnit();

    std::optional<Die> FirstDieInCompilationUnit();

    std::optional<Die> DieByOffset(uint64_t offset);

    ~Dwarf();

private:
	Dwarf_Debug _dbg = 0;
	int _fd = -1;
};

}