#pragma once

#include "../DebugInfoParser.hpp"
#include "../../types.hpp"
#include "dwarf_ptr.hpp"

#include <libdwarf.h>

#include <string>
#include <optional>


namespace meta::details
{

enum class DieType
{
    Unknown,
    CompilationUnit,
    Class,
    Struct,
    Union,
    Subprogram,
    Variable,
    Pointer,
    Reference,
    SubroutineType,
    Const,
    Voaltile,
    Type,
    Typedef,
    Namespace,
};

class Die{
public:
    Die(Dwarf_Debug dbg, dwarf_ptr<Dwarf_Die> die);

    std::optional<Die> FirstChild() const;

    std::optional<Die> Sibling() const;

    DieType Type() const;

    std::string Name() const;

    uint64_t Offset() const;

    size_t Size() const;

    Accessibility Access() const;

    uint64_t TypeDieOffset() const;

    const SourceLocation Location() const;

    size_t DataMemberOffset() const;

    uint64_t SubroutineEntry() const;

    uint64_t SubroutineDeclOffset() const;

private:
    Die CompilationUnitDie() const;
private:
    Dwarf_Debug _dbg;
    dwarf_ptr<Dwarf_Die> _die;
    Dwarf_Half _tag;
    DieType _type = DieType::Unknown;
    std::string _name;
    Dwarf_Off _offset;
};

}