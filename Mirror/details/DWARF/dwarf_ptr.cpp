#include "dwarf_ptr.hpp"

namespace meta::details
{

template <> dwarf_ptr<Dwarf_Die> make_dwarf_ptr<Dwarf_Die>(Dwarf_Debug dbg, Dwarf_Die d)
{
    return dwarf_ptr(dbg, d, DW_DLA_DIE);
}

}