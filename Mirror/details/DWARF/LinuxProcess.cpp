#include "LinuxProcess.hpp"

#include <fstream>
#include <cstdio>
#include <filesystem>
#include <inttypes.h>
#include <limits.h>

using namespace std::filesystem;
using namespace std::string_literals;

LinuxProcess::LinuxProcess()
{

}

void LinuxProcess::loadModules()
{
    std::ifstream mapsfile("/proc/self/maps");
    while (mapsfile)
    {
        char buf[256];
        mapsfile.getline(buf, sizeof(buf));
        uint64_t addr_from, addr_to;
        char perm_r, perm_w, perm_x, perm_cow;
        uint64_t mmap_offset;
        int dev_maj, dev_min;
        uint64_t inode;
        static char path[PATH_MAX];
        static const char* formatstr = "%" PRIx64 "-" "%" PRIx64 " %c%c%c%c %" PRIx64 " %x:%x %" PRIx64 " %s";
        sscanf(buf, formatstr, 
                    &addr_from, &addr_to,
                    &perm_r, &perm_w, &perm_x, &perm_cow,
                    &mmap_offset,
                    &dev_maj, &dev_min,
                    &inode,
                    path
                    );

        if (perm_x == 'x')
        {
            if (path != "[vdso]"s)
            {
                _modules.push_back({path, addr_from - mmap_offset});
            }
        }

    }
}