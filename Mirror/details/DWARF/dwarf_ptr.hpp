#pragma once

#include <memory>
#include <functional>
#include <libdwarf.h>

#define SHARED_PTR_OF(_T) std::shared_ptr<typename std::remove_pointer<_T>::type>

namespace meta::details
{
template<typename T> 
class dwarf_ptr : private SHARED_PTR_OF(T)
{
public:
    dwarf_ptr(Dwarf_Debug dbg, T p, unsigned deallocate_type)
    :SHARED_PTR_OF(T)(p, std::bind(deleter, dbg, deallocate_type, std::placeholders::_1)) 
    {

    }
    using SHARED_PTR_OF(T)::get;
    using SHARED_PTR_OF(T)::operator->;
    using SHARED_PTR_OF(T)::operator bool;

    operator const T() const
    {
        return get();
    }

private:
    static void deleter(Dwarf_Debug dbg, unsigned dealloc_type, T p)
    {
        dwarf_dealloc(dbg, p, dealloc_type);
    }
};

template <typename T> dwarf_ptr<T> make_dwarf_ptr(Dwarf_Debug, T);

template <> dwarf_ptr<Dwarf_Die> make_dwarf_ptr<Dwarf_Die>(Dwarf_Debug dbg, Dwarf_Die d);

}