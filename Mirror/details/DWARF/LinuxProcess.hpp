#include "../Process.hpp"


class LinuxProcess : public Process
{
public:
    LinuxProcess();

protected:
    virtual void loadModules();

};