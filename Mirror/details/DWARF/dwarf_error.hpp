#include "../../types.hpp"
#include <string>

#include <libdwarf.h>

namespace meta::details
{

class dwarf_error : public reflection_error
{
public:
    dwarf_error(const std::string& what, Dwarf_Debug dbg, Dwarf_Error err)
    :reflection_error(what+":"+dwarf_errmsg(err))
    {
        if (dbg)
        {
            dwarf_dealloc(dbg, err, DW_DLA_ERROR);
        }
    }

};

}