#pragma once

#include "../types.hpp"

#include <string>
#include <vector>

namespace meta
{

namespace details
{

struct TypeTraits
{
    bool Pointer = false;
    bool Reference = false;
    bool Const = false;
    bool Volatile = false;
    bool Typedef = false;
};

class DebugInfoFetcher{
public:
    virtual std::string Name(uint64_t dbginfoid) = 0;
    virtual size_t Size(uint64_t dbginfoid) = 0;
    virtual Accessibility Access(uint64_t dbginfoid) = 0;
    virtual std::vector<uint64_t> Methods(uint64_t dbginfoid) = 0;
    virtual std::vector<uint64_t> Members(uint64_t dbginfoid) = 0;
    virtual size_t MemberOffset(uint64_t dbginfoid) = 0;

    virtual uint64_t TypeOf(uint64_t dbginfoid) = 0;

    virtual bool TypeIsSubroutine(uint64_t dbginfoid) = 0;
    virtual uint64_t SubroutineEntry(uint64_t dbginfoid) = 0;

    virtual std::vector<uint64_t> Arguments(uint64_t dbginfoid) = 0;

    virtual TypeTraits Traits(uint64_t dbginfoid) = 0;

};


class DebugInfoParser : public DebugInfoFetcher{
public:
    virtual uint64_t getClassDbgInfoId(const std::string& class_name, bool name_is_mangled = true) = 0;

    virtual ~DebugInfoParser() {}

};

}
}