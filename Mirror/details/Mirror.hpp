#pragma once

#include <string>
#include "../Class.hpp"
#include "../Variable.hpp"

namespace meta::details
{
    class Mirror {
    public:
        static Mirror& Instance();
    private:
        Mirror();

        Class MetaclassFor(const std::string&, bool name_is_mangled = true);
        Class MetaclassFor(const std::string&, void* This, bool name_is_mangled = true);

        Type TypeFor(const std::type_info&);

        Variable VariableFor(const std::type_info&, void* This);

        void LoadModules();

        friend Class meta_cast_class_by_name(const std::string& type_name);
        template<typename T> friend ClassOf<T> meta_cast_class();
        template<typename T> friend ClassOf<T> meta_cast_class(T&);
        template<typename T> friend Type meta_cast_type();
        template<typename T> friend Variable meta_cast_variable(T&);
    private:
        struct Impl;
        static void DeleteImpl(Impl*);
        std::unique_ptr<Impl, decltype(&DeleteImpl)> _pImpl;
    };

    template<typename T>
    ClassOf<T> meta_cast_class()
    {
        return Mirror::Instance().MetaclassFor(typeid(T).name());
    }

    template<typename T>
    ClassOf<T> meta_cast_class(T& obj)
    {
        return Mirror::Instance().MetaclassFor(typeid(obj).name(), &obj);
    }

    Class meta_cast_class_by_name(const std::string& type_name);

    template<typename T>
    Type meta_cast_type()
    {
        return Mirror::Instance().TypeFor(typeid(T));
    }

    template<typename T>
    Variable meta_cast_variable(T& obj)
    {
        return Mirror::Instance().VariableFor(typeid(T), &obj);
    }

    void Initialize();
}