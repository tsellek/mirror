#pragma once

#include "MetaObject.hpp"

namespace meta
{

class Type: public MetaObject
{
public:
    Type(uint64_t dbginfoid, details::DebugInfoFetcher*);

    std::string Name() const override;

    bool IsConst() const;
    bool IsVolatile() const;
    bool IsPointer() const;
    bool IsReference() const;
    bool IsTypedef() const;

    const Type UnderlyingType() const;
};

extern const Type NoType;

}