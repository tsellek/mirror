#pragma once

#include "MetaObject.hpp"
#include "Type.hpp"

namespace meta
{

class DataIdentifier : public MetaObject
{
public:
    DataIdentifier(uint64_t dbginfoid, details::DebugInfoFetcher* fetcher);
    virtual const Type GetType() const;
    template<typename T> T& ValueAs() const
    {
        return *(T*)Address();
    }

protected:
    virtual void* Address() const = 0;
};

}