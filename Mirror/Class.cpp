#include "Class.hpp"
#include "details/DebugInfoParser.hpp"

namespace meta
{
    using namespace details;

    Class::Class(void* This, uint64_t dbginfoid, DebugInfoFetcher* fetcher)
    :Type(dbginfoid, fetcher), _This(This)
    {

    }
    Class::Class(uint64_t dbginfoid, DebugInfoFetcher* fetcher)
    :Class(nullptr, dbginfoid, fetcher)
    {
    }

    const std::vector<Method> Class::Methods(const std::string& name) const
    {
        std::vector<Method> ret;
        for (auto id: _infofetcher->Methods(_dbginfoid))
        {
            Method m{_This, id, _infofetcher};
            std::string mname = m.Name();

            if ( name.empty() || 
                (mname == name))
            {
                ret.emplace_back(_This, id, _infofetcher);
            }
        }

        return ret;
    }

    const std::vector<Member> Class::Members() const
    {
        std::vector<meta::Member> ret;
        for (auto id: _infofetcher->Members(_dbginfoid))
        {
            ret.emplace_back(_This, id, _infofetcher);
        }

        return ret;
    }

    const std::optional<Member> Class::MemberByName(const std::string& name) const
    {
        for (auto id: _infofetcher->Members(_dbginfoid))
        {
            meta::Member m{_This, id, _infofetcher};
            if (m.Name() == name)
            {
                return m;
            }
        }

        return std::optional<meta::Member>{};
    }

}