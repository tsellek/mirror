#pragma once

#include "DataIdentifier.hpp"

namespace meta
{

class Member : public DataIdentifier
{
public:
    Member(void* This, uint64_t dbginfoid, details::DebugInfoFetcher* fetcher);

protected:
    virtual void* Address() const override;

private:
    void* _This;
};

}