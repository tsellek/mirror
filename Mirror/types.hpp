#pragma once

#include <ostream>
#include <filesystem>
#include <stdexcept>

namespace meta
{

enum class Accessibility
{
    Unknown,
    Public,
    Protected,
    Private
};

struct SourceLocation
{
    std::filesystem::path file;
    uint64_t line;
};


class reflection_error : public std::runtime_error
{
public:
    reflection_error(const std::string&);
};

std::ostream& operator<<(std::ostream&, Accessibility);
std::ostream& operator<<(std::ostream&, const SourceLocation&);
bool operator==(const SourceLocation&, const SourceLocation&);
bool operator!=(const SourceLocation&, const SourceLocation&);
bool operator<(const SourceLocation&, const SourceLocation&);

}