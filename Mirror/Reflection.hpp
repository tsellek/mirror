#pragma once

#include "details/Mirror.hpp"

#include <string>
#include <type_traits>
#include "Class.hpp"
#include "Type.hpp"
#include "Variable.hpp"

namespace meta
{

template<typename T, typename =std::enable_if_t<std::is_class_v<T>> >
ClassOf<T>
meta_cast()
{
    return details::meta_cast_class<T>();
}

template<typename T, typename =std::enable_if_t<std::is_class_v<T>> >
ClassOf<T>
meta_cast(T& obj)
{
    return details::meta_cast_class<T>(obj);
}

template <typename T, std::enable_if_t<std::is_convertible_v<T, const std::string&>, bool> = true>
Class
meta_cast(T type_name)
{
    return details::meta_cast_class_by_name(type_name);
}

template<typename T, std::enable_if_t<std::is_fundamental_v<T>, bool> = true>
Type
meta_cast()
{
    return details::meta_cast_type<T>();
}

template<typename T, typename =std::enable_if_t<std::is_fundamental_v<T>> >
Variable
meta_cast(T& obj)
{
    return details::meta_cast_variable<T>(obj);
}

}