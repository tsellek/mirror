#pragma once

#include "DataIdentifier.hpp"

namespace meta
{

class Parameter : public DataIdentifier
{
public:
    Parameter(uint64_t dbginfoid, details::DebugInfoFetcher* fetcher);
protected:
    virtual void* Address() const override;
};

}