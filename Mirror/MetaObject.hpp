#pragma once

#include "details/DebugInfoParser.hpp"

#include <string>

namespace meta
{

class MetaObject{
public:
    MetaObject(uint64_t dbginfoid, details::DebugInfoFetcher*);

    virtual std::string Name() const;
    virtual size_t Size() const;
    virtual Accessibility Access() const;

    virtual ~MetaObject();

protected:
    details::DebugInfoFetcher* _infofetcher = nullptr;
    const uint64_t _dbginfoid = 0;
};

}