#include "Parameter.hpp"

namespace meta
{

    Parameter::Parameter(uint64_t dbginfoid, details::DebugInfoFetcher* fetcher)
    :DataIdentifier(dbginfoid, fetcher)
    {

    }

    void* Parameter::Address() const
    {
        throw reflection_error("Trying to take the address or value of a formal parameter");
    }
}