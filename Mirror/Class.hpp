#pragma once

#include "Type.hpp"
#include "Method.hpp"
#include "Member.hpp"

#include <optional>

namespace meta
{

class Class : public Type{
public:
    Class(uint64_t dbginfoid, details::DebugInfoFetcher*);
    Class(void* This, uint64_t dbginfoid, details::DebugInfoFetcher*);

    const std::vector<Method> Methods(const std::string& name="") const;
    const std::vector<Member> Members() const;
    const std::optional<Member> MemberByName(const std::string& name) const;
private:
    void* _This;
};

template<typename ClassType>
class ClassOf
{
public:
   ClassOf(Class c)
    :_class(c)
    {}

    virtual std::string Name() const
    {
        return _class.Name();
    }
    virtual size_t Size() const
    {
        return _class.Size();   
    }
    const std::vector< Method > Methods(const std::string& name="") const
    {
        return _class.Methods(name);
    }
    const std::vector<Member> Members() const
    {
        return _class.Members();
    }

    const std::optional<Member> MemberByName(const std::string& name) const
    {
        return _class.MemberByName(name);
    }

    bool IsConst() const
    {
        return _class.IsConst();
    }

    bool IsVolatile() const
    {
        return _class.IsVolatile();
    }

    bool IsPointer() const
    {
        return _class.IsPointer();
    }

    bool IsReference() const
    {
        return _class.IsReference();
    }

    bool IsTypedef() const
    {
        return _class.IsTypedef();
    }

    const Type UnderlyingType() const
    {
        return _class.UnderlyingType();
    }

    const Class& GetUntyped() const
    {
        return _class;
    }

    template<class... Args>
    ClassType* Create(Args&&... args)
    {
        return new ClassType(args...);
    }

private:
    Class _class;
};

}