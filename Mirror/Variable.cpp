#include "Variable.hpp"

namespace meta
{
    Variable::Variable(void* addr, uint64_t dbginfoid, details::DebugInfoFetcher* fetcher)
    :DataIdentifier(0, nullptr), _addr(addr), _type(dbginfoid, fetcher)
    {
        if (!_addr)
        {
            throw reflection_error("Trying to get the value of a variable with a NULL address");
        }        
    }

    const Type Variable::GetType() const
    {
        return _type;
    }

    void* Variable::Address() const
    {
        return _addr;
    }

}