#include "Method.hpp"

namespace meta
{

using namespace details;

Method::Method(void* This, uint64_t dbginfoid, DebugInfoFetcher* fetcher)
:MetaObject(dbginfoid, fetcher), _This(This)
{

}

Type Method::ReturnType() const
{
    try
    {
        uint64_t type_dbginfoid = _infofetcher->TypeOf(_dbginfoid);
        return Type(type_dbginfoid, _infofetcher);
    }
    catch(const std::exception&)
    {
        return NoType;
    }
    
}

std::vector<Parameter> Method::Arguments() const
{
    std::vector<Parameter> ret;
    for (auto id: _infofetcher->Arguments(_dbginfoid))
    {
        ret.emplace_back(id, _infofetcher);
    }

    return ret;
}

}