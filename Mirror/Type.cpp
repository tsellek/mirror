#include "Type.hpp"
#include "types.hpp"

namespace meta
{

using namespace details;

const Type NoType(0, nullptr);

Type::Type(uint64_t dbginfoid, DebugInfoFetcher* fetcher)
:MetaObject(dbginfoid, fetcher)
{

}

std::string Type::Name() const
{
    if (!_infofetcher)
    {
        return "void";
    }

    if (IsPointer() || IsReference())
    {
        std::string pointee_name;
        try {
            uint64_t pointee_type_dbginfoid = _infofetcher->TypeOf(_dbginfoid);
            Type pointee_type =  Type(pointee_type_dbginfoid, _infofetcher);
            pointee_name = pointee_type.Name();
        } catch (reflection_error&) {
            //assuming it's because TypeOf failed
            pointee_name = "void";
        }
        return pointee_name + " " + MetaObject::Name() +(IsConst()?" const":"");

    }
    else if (IsConst())
    {
        return MetaObject::Name() + " const";        
    }
    else if (_infofetcher->TypeIsSubroutine(_dbginfoid))
    {
        uint64_t return_type_dbginoid = _infofetcher->TypeOf(_dbginfoid);
        Type return_type =  Type(return_type_dbginoid, _infofetcher);
        return "<subroutine returning " + return_type.Name() + ">" + MetaObject::Name();        
    }
    else
    {
        return MetaObject::Name();
    }
}


bool Type::IsConst() const
{
    return _infofetcher->Traits(_dbginfoid).Const;
}

bool Type::IsVolatile() const
{
    return _infofetcher->Traits(_dbginfoid).Volatile;
}

bool Type::IsPointer() const
{
    return _infofetcher->Traits(_dbginfoid).Pointer;
}

bool Type::IsReference() const
{
    return _infofetcher->Traits(_dbginfoid).Reference;
}

bool Type::IsTypedef() const
{
    return _infofetcher->Traits(_dbginfoid).Typedef;
}

const Type Type::UnderlyingType() const
{
    return Type(_infofetcher->TypeOf(_dbginfoid), _infofetcher);
}

}